//
//  Helper.swift
//  BitesFunds
//
//  Created by Apple on 12/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import SVProgressHUD
import MMDrawerController

class Helper: NSObject {
    
    static let shared = Helper()
    
    var userId  = ""
    var user_token = ""
    var user_profile_pic = ""
    var user_name = ""
    
    var restaurantId  = ""
    var restaurant_token = ""
    var restaurant_profile_pic = ""
    var restaurant_name = ""
    var restaurant_description = ""
    
    var strLoginType:String = "General"
    
    func showHUD() {
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.custom)
        SVProgressHUD.setBackgroundLayerColor(UIColor.black.withAlphaComponent(0.5))
    }
    
    func userLogout() {
        
        UserDefaults.standard.set(false, forKey: "isUserLogin")
        UserDefaults.standard.set("", forKey: "userId")
        UserDefaults.standard.set("", forKey: "api_token")
        UserDefaults.standard.set("", forKey: "profile_pic")
        UserDefaults.standard.set("", forKey: "userName")
        UserDefaults.standard.synchronize()
        
        Helper.shared.strLoginType = "General"
        
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navIntro") as! UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = obj
    }
    
    func restaurantLogout() {
        
        UserDefaults.standard.set(false, forKey: "isRestLogin")
        UserDefaults.standard.set("", forKey: "restaurant_Id")
        UserDefaults.standard.set("", forKey: "restaurant_token")
        UserDefaults.standard.set("", forKey: "restaurant_image")
        UserDefaults.standard.set("", forKey: "restaurantName")
        UserDefaults.standard.synchronize()
        
        Helper.shared.strLoginType = "General"
        
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navIntro") as! UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = obj
    }
    
    
}

import UIKit
import TezosKit
import KeychainSwift
import os.log

class KeychainWallet {
    public let wallet: Wallet?
    
    private struct keychainKeys {
        static let mnemonic = "mnemonic"
        static let passphrase = "passphrase"
    }
    private static let keychain = KeychainSwift(keyPrefix: "tech.cryptonomic.wallet")
    
    // Constructor used when restoring from keychain.
    private init?(wallet: Wallet) {
        self.wallet = wallet
    }
    
    // Create wallet without supplying mnemonic (this generates a mnemonic randomly).
    public init?(passphrase: String = "chefsclubcollective") {
        // create wallet
        guard let wallet = Wallet(passphrase: passphrase, signingCurve: .ed25519) else {
            os_log(.error, "Error generating wallet")
            return nil
        }
        self.wallet = wallet
        // write to keychain
        let mnemonicResult = KeychainWallet.keychain.set(wallet.mnemonic!, forKey: keychainKeys.mnemonic, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
        let passphraseResult = KeychainWallet.keychain.set(passphrase, forKey: keychainKeys.passphrase, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
        if !mnemonicResult || !passphraseResult {
            os_log(.error, "Error writing wallet to keychain")
            return nil
        }
    }
    
    // Create wallet with given mnemonic (this can be used to restore a wallet).
    public init?(mnemonic: String, passphrase: String = "chefsclubcollective") {
        // create wallet
        guard let wallet = Wallet(mnemonic: mnemonic, passphrase: passphrase, signingCurve: .ed25519) else {
            os_log(.error, "Error generating wallet")
            return nil
        }
        self.wallet = wallet
        // write mnemonic to keychain
        let mnemonicResult = KeychainWallet.keychain.set(mnemonic, forKey: keychainKeys.mnemonic, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
        let passphraseResult = KeychainWallet.keychain.set(passphrase, forKey: keychainKeys.passphrase, withAccess: .accessibleWhenUnlockedThisDeviceOnly)
        if !mnemonicResult || !passphraseResult {
            os_log(.error, "Error writing wallet to keychain")
            return nil
        }
    }
        
    public static func restoreFromKeychain() -> KeychainWallet? {
        // read mnemonic from keychain
        guard
            let mnemmonic = KeychainWallet.keychain.get(keychainKeys.mnemonic),
            let passphrase = KeychainWallet.keychain.get(keychainKeys.passphrase) else {
            os_log(.error, "Error recovering wallet from keychain")
            return nil
        }
        // create KeychainWallet
        return KeychainWallet(mnemonic: mnemmonic, passphrase: passphrase)
    }
    
    public func getMnemonic() -> String? {
        return wallet?.mnemonic
    }
    
    public func getAddress() -> Address? {
        return wallet?.address
    }
    
}

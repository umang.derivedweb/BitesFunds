//
//  Constants.swift
//  Hungriji
//
//  Created by 360Technosoft on 04/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit



struct URLs {
    static let baseurl = "https://www.chefsclubcollective.org/api/"
    
    static let get_support_restaurent = baseurl + "get-support-restaurent"
    static let get_restaurent = baseurl + "get-restaurent"
    static let restaurant_details = baseurl + "restaurant-details?restaurant_id="
    static let get_states = baseurl + "restaurant/get-states"
    
    static let user_register = baseurl + "user/register"
    static let user_login = baseurl + "user/login"
    static let user_forgot_password = baseurl + "user/forgot-password"
    static let user_check_otp = baseurl + "user/check-otp"
    static let user_new_password = baseurl + "user/new-password"
    static let user_create_voucher = baseurl + "user/create-voucher"
    static let user_get_bite_wallet = baseurl + "user/get-bite-in-wallet"
    static let user_dashboard = baseurl + "user/dashboard"
    static let user_voucher_detail = baseurl + "user/voucher-detail?voucher_id="
    static let user_get_profile = baseurl + "user/get-profile"
    static let user_update_profile = baseurl + "user/update-profile"
    static let user_change_password = baseurl + "user/change-password"
    static let get_cities = baseurl + "restaurant/get-cities"
    static let user_transaction_history = baseurl + "user/transaction-history"
    static let user_redeemed_voucher_list = baseurl + "user/redeemed-voucher-list"
    static let get_usd_paid_bite_bought_list = baseurl + "user/get-usd-paid-bite-bought-list"
    static let get_usd_paid_bite_bought = baseurl + "user/get-usd-paid-bite-bought"
    static let buy_bite = baseurl + "user/buy-bite"
    static let contact_us = baseurl + "contact-us"
    
    static let restaurant_register = baseurl + "restaurant/register"
    static let restaurant_login = baseurl + "restaurant/login"
    static let restaurant_forgot_password = baseurl + "restaurant/forgot-password"
    static let restaurant_check_otp = baseurl + "restaurant/check-otp"
    static let restaurant_new_password = baseurl + "restaurant/new-password"
    static let restaurant_dashboard = baseurl + "restaurant/dashboard"
    static let restaurant_voucher_detail = baseurl + "restaurant/voucher-detail?voucher_id="
    static let restaurant_get_profile = baseurl + "restaurant/get-profile"
    static let restaurant_change_password = baseurl + "restaurant/change-password"
    static let restaurant_update_profile = baseurl + "restaurant/update-profile"
    static let get_restaurant_type = baseurl + "restaurant/get-restaurant-type"
    static let restaurant_transaction_history = baseurl + "restaurant/transaction-history"
    static let restaurant_redeemed_voucher_list = baseurl + "restaurant/redeemed-voucher-list"
    static let get_total_funds_raised_list = baseurl + "restaurant/get-total-funds-raised-list"
    static let restaurant_redeem_voucher = baseurl + "restaurant/redeem-voucher"
    static let restaurant_get_total_funds_raised = baseurl + "restaurant/get-total-funds-raised"
}

struct Message {
    
    static let enterFName = "Please enter First Name"
    static let enterDesc = "Please enter description"
    static let enterLName = "Please enter Last Name"
    static let enterValidEmail = "Please enter valid Email"
    static let enterEmail = "Please enter Email"
    static let enterValidNumber = "Please enter Valid Mobile Number"
    static let enterNumber = "Please enter Mobile Number"
    static let enterSex = "Please enter Sex"
    static let enterDOB = "Please enter Date of birth"
    static let enterPassword = "Please enter password"
    static let passwordUnmatched = "Password doesn't match"
    static let enterRestaurant = "Please enter Restaurant name"
    static let selectCity = "Please select City"
    static let selectRestType = "Please select Restaurant Type"
}

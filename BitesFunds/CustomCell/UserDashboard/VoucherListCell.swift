//
//  VoucherListCell.swift
//  BitesFunds
//
//  Created by Apple on 08/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class VoucherListCell: UITableViewCell {

    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var lblVoucherCode: UILabel!
    @IBOutlet weak var lblBite: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnActive: UIButton!
    @IBOutlet weak var btnOpenVoucher: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBack.shadowToView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

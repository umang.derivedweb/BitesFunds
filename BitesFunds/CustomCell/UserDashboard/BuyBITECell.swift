//
//  BuyBITECell.swift
//  BitesFunds
//
//  Created by Apple on 13/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BuyBITECell: UITableViewCell {

    @IBOutlet weak var lblFourth: UILabel!
    @IBOutlet weak var lblThird: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var lblFirst: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

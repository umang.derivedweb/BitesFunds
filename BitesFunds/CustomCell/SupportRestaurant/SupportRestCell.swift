//
//  SupportRestCell.swift
//  BitesFunds
//
//  Created by Apple on 09/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class SupportRestCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblCity: UILabel!
    override func awakeFromNib() {
        viewBack.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        viewBack.layer.borderWidth = 0.5
    }
}

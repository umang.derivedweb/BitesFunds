//
//  AboutUsCell.swift
//  BitesFunds
//
//  Created by Apple on 10/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AboutUsCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        
        self.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.layer.borderWidth = 0.5
    }
}

//
//  FAQCell.swift
//  BitesFunds
//
//  Created by Apple on 10/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class FAQCell: UITableViewCell {

    @IBOutlet weak var lblQue: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblAns: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

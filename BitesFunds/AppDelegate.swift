//
//  AppDelegate.swift
//  BitesFunds
//
//  Created by Apple on 08/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import MMDrawerController
import Stripe
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    var centerContainer: MMDrawerController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
       
        IQKeyboardManager.shared.enable = true
        Stripe.setDefaultPublishableKey("pk_test_ZKkMvqQa8kgaYQ7xpEdnj77Q00F6B49ZPh")
        
        
        
        GMSServices.provideAPIKey("AIzaSyAliRXfmWvBE5VhYJPlxqTnU-ULpkUoF38")
    
        
        /*let centerViewController = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "UserDashboardVC") as! UserDashboardVC
        let leftViewController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "GeneralMenuVC") as! GeneralMenuVC
        
        let leftSideNav = UINavigationController(rootViewController: leftViewController)
        leftSideNav.navigationBar.isHidden = true
        let centerNav = UINavigationController(rootViewController: centerViewController)
        centerNav.navigationBar.isHidden = true
        centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
        
        centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
        centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapCenterView
        //window?.rootViewController = centerContainer
        centerContainer?.maximumLeftDrawerWidth = 350
        //window?.makeKeyAndVisible()*/
        
        return true
    }

    

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "BitesFunds")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}


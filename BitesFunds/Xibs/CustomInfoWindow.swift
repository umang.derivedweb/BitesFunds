//
//  CustomInfoWindow.swift
//  BitesFunds
//
//  Created by Apple on 24/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CustomInfoWindow: UIView {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnViewDetails: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var backView: UIView!

    override func awakeFromNib() {
        
    }
}

//
//  UserDashboardVC.swift
//  BitesFunds
//
//  Created by Apple on 08/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import MMDrawerController

class UserDashboardVC: UIViewController {

    // MARK: - Variables
    var arrVoucher:[VoucherListData] = []
    var arrSearchedVoucher:[VoucherListData]?
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var lblWalletBite: UILabel!
    @IBOutlet weak var lblActiveVoucher: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblBiteinVoucher: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        dashboardAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        viewLogout.shadowToView()
        table.rowHeight = 111
        navigationController?.navigationBar.isHidden = true
    }
    @objc func btnUserLogin() {
        //tabBarController?.view.viewWithTag(1001)?.removeFromSuperview()
        self.view.viewWithTag(1001)?.removeFromSuperview()
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        Helper.shared.strLoginType = "User"
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func btnRestLogin() {
        //tabBarController?.view.viewWithTag(1001)?.removeFromSuperview()
        self.view.viewWithTag(1001)?.removeFromSuperview()
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        Helper.shared.strLoginType = "Rest"
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
    @objc func btnClose() {
        //tabBarController?.view.viewWithTag(1001)?.removeFromSuperview()
        self.view.viewWithTag(1001)?.removeFromSuperview()
    }
    
    //MARK: - Webservice
    func dashboardAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.user_token)"]
        NetworkManager.shared.webserviceCallUserDashboard(url: "\(URLs.user_dashboard)?page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.last_page = response.data?.vouchers?.last_page ?? 0
                self.lblWalletBite.text = response.data?.bite_in_wallet
                self.lblBiteinVoucher.text = response.data?.total_active_voucher_bite
                self.lblActiveVoucher.text = "BITE in \(response.data?.active_voucher ?? "0") active vouchers"
                self.arrVoucher += (response.data?.vouchers?.data ?? [])
                self.arrSearchedVoucher = self.arrVoucher
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    
    @IBAction func btnVoucherHistory(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RedeemedVoucherListVC") as! RedeemedVoucherListVC
        obj.isFrom = "Dashboard"
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
    @IBAction func btnLogout(_ sender: Any) {
        Helper.shared.userLogout()
        
    }
    @IBAction func btnSearch(_ sender: Any) {
        
        if txtSearch.text?.isEmpty ?? true
        {
            return
        }
        
        arrSearchedVoucher = arrVoucher.filter { ($0.restaurant_name?.localizedCaseInsensitiveContains(txtSearch?.text ?? ""))! }
        
        self.table.reloadData()
    }
    
    @IBAction func btnBiteInVoucher(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "TransactionHistoryVC") as! TransactionHistoryVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnTabbar(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            let obj = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "FindRestaurantVC") as! FindRestaurantVC
            obj.isShowPopup = true
            navigationController?.pushViewController(obj, animated: false)
        } else if sender.tag == 2 {
            
            let obj = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "FindRestaurantVC") as! FindRestaurantVC
            obj.isShowPopup = false
            navigationController?.pushViewController(obj, animated: false)
        }
    }
    
}
extension UserDashboardVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchedVoucher?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherListCell") as! VoucherListCell
        
        let dict = arrSearchedVoucher?[indexPath.row]
        cell.lblRestName.text = dict?.restaurant_name
        cell.lblBite.text = dict?.bite
        cell.lblDate.text = dict?.date
        cell.lblVoucherCode.text = dict?.voucher_code
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "VoucherDetailsVC") as! VoucherDetailsVC
        obj.strVoucherId = "\(arrSearchedVoucher?[indexPath.row].voucher_id ?? 0)"
        navigationController?.pushViewController(obj, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrSearchedVoucher?.count {
            if (indexPath.row == (self.arrSearchedVoucher?.count ?? 0) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrSearchedVoucher?.count ?? 0
                    dashboardAPI()
                }
                
            }
        }
    }
}
extension UserDashboardVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength = (textField.text!.count) + (string.count) - range.length

        if newLength == 0 {
            arrSearchedVoucher = arrVoucher
            table.reloadData()
        }
        
        return true
    }
}

//
//  RedeemedVoucherListVC.swift
//  BitesFunds
//
//  Created by Apple on 09/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class RedeemedVoucherListVC: UIViewController {

    // MARK: - Variables
    var arrRedeemedVoucher:[RedeemedVouchersList] = []
    var arrSearchedRedeemedVoucher:[RedeemedVouchersList]?
    var isFrom:String?
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var lblTotalBite: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        redeemedVoucherListAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        viewLogout.shadowToView()
        navigationController?.navigationBar.isHidden = true
        table.rowHeight = 75
        
        if isFrom == "Dashboard" {
            btnMenu.setImage(#imageLiteral(resourceName: "left"), for: .normal)
        }
    }
    
    //MARK: - Webservice
    func redeemedVoucherListAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.user_token)"]
        NetworkManager.shared.webserviceCallReedemedVoucherList(url: "\(URLs.user_redeemed_voucher_list)?page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.last_page = response.data?.redeemed_vouchers?.last_page ?? 0
                self.lblTotalBite.text = "\(response.data?.total_redeemed_bite ?? "0") BITE in \(response.data?.redeemed_vouchers?.total ?? 0) redeemed vouchers"
                self.arrRedeemedVoucher += (response.data?.redeemed_vouchers?.data ?? [])
                self.arrSearchedRedeemedVoucher = self.arrRedeemedVoucher
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnMenu(_ sender: Any) {
        if isFrom == "Dashboard" {
            navigationController?.popViewController(animated: true)
        } else {
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
    @IBAction func btnLogout(_ sender: Any) {
        Helper.shared.userLogout()
    }
    @IBAction func btnSearch(_ sender: Any) {
        
        if txtSearch.text?.isEmpty ?? true
        {
            return
        }
        
        arrSearchedRedeemedVoucher = arrRedeemedVoucher.filter { ($0.restaurant_name?.localizedCaseInsensitiveContains(txtSearch?.text ?? ""))! }
        
        self.table.reloadData()
    }

}
extension RedeemedVoucherListVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchedRedeemedVoucher?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RedeemedVoucherCell") as! RedeemedVoucherCell
        let dict = arrSearchedRedeemedVoucher?[indexPath.row]
        cell.lblFirst.text = dict?.restaurant_name
        cell.lblSecond.text = dict?.voucher_code
        cell.lblThird.text = dict?.bite
        cell.lblFourth.text = dict?.date_added
        cell.btnStatus.setTitle(dict?.status, for: .normal)
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrSearchedRedeemedVoucher?.count {
            if (indexPath.row == (self.arrSearchedRedeemedVoucher?.count ?? 0) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrSearchedRedeemedVoucher?.count ?? 0
                    redeemedVoucherListAPI()
                }
                
            }
        }
    }
}
extension RedeemedVoucherListVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength = (textField.text!.count) + (string.count) - range.length

        if newLength == 0 {
            self.arrSearchedRedeemedVoucher = self.arrRedeemedVoucher
            table.reloadData()
        }
        
        return true
    }
}

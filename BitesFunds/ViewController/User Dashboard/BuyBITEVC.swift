//
//  BuyBITEVC.swift
//  BitesFunds
//
//  Created by Apple on 13/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class BuyBITEVC: UIViewController {

    // MARK: - Variables
    var arrBiteBoughtList:[BiteBoughtDataListData] = []
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var viewLogout: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        biteBoughtListAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        viewLogout.shadowToView()
        navigationController?.navigationBar.isHidden = true
        table.rowHeight = 75
    }
    //MARK: - Webservice
    func biteBoughtListAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.user_token)"]
        NetworkManager.shared.webserviceCallBiteBoughtList(url: "\(URLs.get_usd_paid_bite_bought_list)?page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.last_page = response.data!.last_page ?? 0
                self.arrBiteBoughtList += (response.data?.data ?? [])
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    // MARK: - IBAction
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
    @IBAction func btnLogout(_ sender: Any) {
        Helper.shared.userLogout()
    }

}
extension BuyBITEVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBiteBoughtList.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuyBITECell") as! BuyBITECell
        let dict = arrBiteBoughtList[indexPath.row]
        cell.lblFirst.text = dict.date
        cell.lblSecond.text = dict.restaurant_name
        cell.lblThird.text = dict.usd_spent
        cell.lblFourth.text = dict.bite
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrBiteBoughtList.count {
            if (indexPath.row == (self.arrBiteBoughtList.count ?? 0) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrBiteBoughtList.count ?? 0
                    biteBoughtListAPI()
                }
                
            }
        }
    }
}



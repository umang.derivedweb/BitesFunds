//
//  VoucherDetailsVC.swift
//  BitesFunds
//
//  Created by Apple on 09/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol VoucherDetailsDelegate:class {
    func voucherDetailsFailed(msg:String)
}

class VoucherDetailsVC: UIViewController {

    // MARK: - Variables
    var strVoucherId:String?
    var strType:String?
    var delegate:VoucherDetailsDelegate?
    
    // MARK: - IBoutlet
    @IBOutlet weak var imgRest: UIImageView!
    @IBOutlet weak var lblVoucherId: UILabel!
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var lblOffer: UILabel!
    @IBOutlet weak var lblRestUserName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnPrint: UIButton!
    @IBOutlet weak var viewVoucher: UIView!
    @IBOutlet weak var lblVoucherStatus: UILabel!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var lblPercentage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        viewLogout.shadowToView()
        navigationController?.navigationBar.isHidden = true
        
        if strType == "Rest" {
            
            getVoucherDetailsAPI(url: URLs.restaurant_voucher_detail, token: Helper.shared.restaurant_token)
        } else {
            
            getVoucherDetailsAPI(url: URLs.user_voucher_detail, token: Helper.shared.user_token)
        }
    }
    

    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    
    //MARK: - Webservice
    func getVoucherDetailsAPI(url:String, token:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(token)"]
        NetworkManager.shared.webserviceCallUserVoucherDetails(url: "\(url)\(strVoucherId ?? "")", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblDate.text = response.data?.date_added
                self.lblOffer.text = response.data?.offer
                
                if response.data?.percentage_you_want != nil {
                    self.lblPercentage.text = "Pay up to \(response.data?.percentage_you_want ?? "")% of the check in BITE"
                }
                
                self.lblValue.text = "\(response.data?.bite ?? "") BITE"
                
                self.lblVoucherId.text = response.data?.voucher_code
                
                if response.data?.status == "active" {
                    self.viewVoucher.backgroundColor = #colorLiteral(red: 0.3353324533, green: 0.6541657448, blue: 0.1959112883, alpha: 1)
                    self.lblVoucherStatus.text = "Voucher is Active"
                } else {
                    self.viewVoucher.backgroundColor = #colorLiteral(red: 0.8697224259, green: 0.1745362282, blue: 0.2193457186, alpha: 1)
                    self.lblVoucherStatus.text = "Voucher Already Redeemed"
                }
                
                
                if self.strType == "Rest" {
                    
                    self.lblRestName.text = "\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")"
                    self.lblRestUserName.text = "USER NAME:"
                    self.imgRest?.sd_setImage(with: URL(string: response.data?.profile_pic ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
                    
                    if response.data?.status == "active" {
                        self.btnPrint.setTitle("REDEEM", for: .normal)
                        self.btnPrint.backgroundColor = #colorLiteral(red: 0.8697224259, green: 0.1745362282, blue: 0.2193457186, alpha: 1)
                    } else {
                        self.btnPrint.isHidden = true
                    }
                } else {
                    self.lblRestName.text = response.data?.restaurant_name
                    self.imgRest?.sd_setImage(with: URL(string: response.data?.restaurant_image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
                    
                    
                }
                
                self.imgQR.image = self.generateQRCode(from: response.data?.qr_url ?? "")
            } else {
                
                if response.ResponseCode == 404 {
                    
                    self.delegate?.voucherDetailsFailed(msg: response.ResponseMsg ?? "")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    Toast.show(message: response.ResponseMsg ?? "", controller: self)
                }
                
            }
            SVProgressHUD.dismiss()
        }
    }
    func redeemVoucherAPI() {
        Helper.shared.showHUD()
        
        let params = ["voucher_id":strVoucherId!]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        NetworkManager.shared.webserviceCallCommon(url: URLs.restaurant_redeem_voucher, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.btnPrint.isHidden = true
                self.viewVoucher.backgroundColor = #colorLiteral(red: 0.8697224259, green: 0.1745362282, blue: 0.2193457186, alpha: 1)
                self.lblVoucherStatus.text = "Voucher Already Redeemed"
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPrint(_ sender: Any) {
        
        if self.strType == "Rest" {
            redeemVoucherAPI()
        }
        
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
    @IBAction func btnLogout(_ sender: Any) {
        if strType == "Rest" {
            
            Helper.shared.restaurantLogout()
        } else {
            
            Helper.shared.userLogout()
        }
    }

    
}

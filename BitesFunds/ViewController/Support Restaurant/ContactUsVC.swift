//
//  ContactUsVC.swift
//  BitesFunds
//
//  Created by Apple on 10/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class ContactUsVC: UIViewController {

    // MARK: - Variables
    
    // MARK: - IBOutlet
    @IBOutlet weak var txtFName: FormTextField!
    @IBOutlet weak var txtLName: FormTextField!
    @IBOutlet weak var txtPhone: FormTextField!
    @IBOutlet weak var txtEmail: FormTextField!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var txtview: UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        viewBack.shadowToView()
        txtview.layer.cornerRadius = 5
        txtview.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        txtview.layer.borderWidth = 1
    }
    
    // MARK: - Webservice
    func contactUsAPI() {
        Helper.shared.showHUD()
        
        let params = ["first_name":txtFName.text ?? "",
            "last_name":txtLName.text ?? "",
            "email":txtEmail.text ?? "",
            "phone":txtPhone.text ?? "",
            "notes":txtview.text ?? ""] as [String : Any]
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallCommon(url: URLs.contact_us, parameters: params, headers:headers) { (response) in
            self.view.viewWithTag(1001)?.removeFromSuperview()
            if response.ResponseCode == 200 {
                
                self.txtFName.text = ""
                self.txtLName.text = ""
                self.txtEmail.text = ""
                self.txtPhone.text = ""
                self.txtview.text = ""
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
            
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        if !(txtEmail.text)!.isValidEmail() {
            Toast.show(message: Message.enterValidEmail, controller: self)
            return
        }
        if !(txtPhone.text)!.isValidContact() {
            Toast.show(message: Message.enterValidNumber, controller: self)
            return
        }
        
        contactUsAPI()
    }
    @IBAction func btnBack(_ sender: Any) {
    }
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }

}

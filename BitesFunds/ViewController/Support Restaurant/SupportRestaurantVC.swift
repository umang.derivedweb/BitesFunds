//
//  SupportRestaurantVC.swift
//  BitesFunds
//
//  Created by Apple on 09/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class SupportRestaurantVC: UIViewController {

    // MARK: - Variables
    var arrRest:[SupportRestaurantData]?
    
    // MARK: - IBOutlet
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var viewLogout: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getSupportRestaurantAPI()
    }
    
    

    // MARK: - Function
    func setupUI() {
        viewLogout.shadowToView()
        
        navigationController?.navigationBar.isHidden = true
    }
    
    
    
    // MARK: - Webservice
    
    func getSupportRestaurantAPI() {
        Helper.shared.showHUD()
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetSupportRest(url: URLs.get_support_restaurent, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrRest = response.data
                
                self.collectionview.reloadData()
            }
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnSeeAll(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "FindRestaurantVC") as! FindRestaurantVC
        obj.strFromVC = "SupportVC"
        obj.isViewTabbarHidden = true
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnBuyBite(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "FindRestaurantVC") as! FindRestaurantVC
        obj.isShowPopup = true
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnBecomePartner(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RestaurantRegisterVC") as! RestaurantRegisterVC
        obj.isHideSignIn = true
        navigationController?.pushViewController(obj, animated: false)
    }
    @IBAction func btnLogout(_ sender: Any) {
        if Helper.shared.strLoginType == "User" {
            Helper.shared.userLogout()
        } else if Helper.shared.strLoginType == "Rest" {
            Helper.shared.restaurantLogout()
        }
        viewLogout.isHidden = true
    }
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
}
extension SupportRestaurantVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrRest?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SupportRestCell", for: indexPath) as! SupportRestCell
        
        let dict = arrRest?[indexPath.row]
        
        cell.img.sd_setImage(with: URL(string: dict?.restaurant_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.lblRestName.text = dict?.restaurant_name
        cell.lblCity.text = dict?.city
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionview.frame.width/2)-10, height: 180)
    }
}

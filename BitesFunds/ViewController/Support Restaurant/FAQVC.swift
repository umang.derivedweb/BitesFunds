//
//  FAQVC.swift
//  BitesFunds
//
//  Created by Apple on 10/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class FAQVC: UIViewController {

    //MARK: Variables
    
    let arrQue = ["What is lorem ipsum?","What is lorem ipsum?","What is lorem ipsum?"]
    let arrAns = ["Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda. ","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."]
    var arrImages = ["dropdown-1","dropdown-1","dropdown-1"]


    var selectedIndex = -1

    // i.e initially no row is selected

    var isExpanded = false
    
    //MARK: IBOutlets
    @IBOutlet weak var tableFAQ: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    //MARK: Function
    
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        tableFAQ.estimatedRowHeight = 56
        tableFAQ.rowHeight = UITableView.automaticDimension
        tableFAQ.tableFooterView = UIView()
    }
    

    //MARK: IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    

}

extension FAQVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrQue.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell") as! FAQCell
        
        cell.lblQue.text = arrQue[indexPath.row]
        cell.lblAns.text = arrAns[indexPath.row]
        cell.imgArrow.image = UIImage(named: arrImages[indexPath.row])
        
        if cell.imgArrow.image == UIImage(named: "dropdown-1") {
            
            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (self.selectedIndex == indexPath.row && isExpanded == true) {
            
            return UITableView.automaticDimension
        } else {
            
            return 65
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<arrImages.count {
            
            arrImages.remove(at: i)
            arrImages.insert("dropdown-1", at: i)
        }
        //tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedIndex == indexPath.row {
            
            if self.isExpanded == false {
                
                self.isExpanded = true
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("dropdown-2", at: indexPath.row)
            } else {
                
                self.isExpanded = false
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("dropdown-1", at: indexPath.row)
            }
        } else {
            
            self.isExpanded = true
            
            self.selectedIndex = indexPath.row
            arrImages.remove(at: indexPath.row)
            arrImages.insert("dropdown-2", at: indexPath.row)
        }
        
        //tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.reloadData()
    }
    
}



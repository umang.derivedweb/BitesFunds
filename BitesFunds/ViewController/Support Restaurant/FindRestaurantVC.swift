//
//  FindRestaurantVC.swift
//  BitesFunds
//
//  Created by Apple on 09/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import GoogleMaps
import SVProgressHUD
import AVFoundation

class FindRestaurantVC: UIViewController, isLogin {
    
    

    // MARK: - Variables
    var arrRest:[RestaurantListData] = []
    var arrSearchedRest:[RestaurantListData]?
    var zoom: Float = 0.0
    var bounds = GMSCoordinateBounds()
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var strFromVC:String = "LeftMenu"
    var isShowPopup:Bool = true
    var isViewTabbarHidden:Bool = false
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnQR: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var mapview: GMSMapView!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var txtNameCity: UITextField!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var heightViewTabbar: NSLayoutConstraint!
    @IBOutlet weak var widthQR: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupUI()
        getRestaurantAPI()
    }
    
    
    // MARK: - Function
    func setupUI() {
        mapview.delegate = self
        navigationController?.navigationBar.isHidden = true
        viewLogout.shadowToView()
        if isShowPopup == true {
            
            let customView = Bundle.main.loadNibNamed("BuyBiteView", owner: self, options: nil)?.first as! BuyBiteView
            customView.tag = 1001
            customView.btnSelectRestaurant.addTarget(self, action: #selector(btnSelectRest), for: .touchUpInside)
            customView.frame = self.view.frame
            self.view.addSubview(customView)
        }
        
        if strFromVC == "LeftMenu" || strFromVC == "LeftMenu" {
            btnMenu.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        } else {
            btnMenu.setImage(#imageLiteral(resourceName: "left"), for: .normal)
        }
        
        if isViewTabbarHidden == true {
            heightViewTabbar.constant = 0
        } else {
            heightViewTabbar.constant = 50
        }
        
        if Helper.shared.strLoginType == "User" {
            widthQR.constant = 0
        }
    }
    
    @objc func btnSelectRest() {
        
        self.view.viewWithTag(1001)?.removeFromSuperview()
    }
    
    func sendData() {
        openScanner()
    }
    
    func openScanner() {
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)

        captureSession.startRunning()
        
    }
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func found(code: String) {
        print(code)
        let myStringArr = code.components(separatedBy: "/")
        
        let obj = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "VoucherDetailsVC") as! VoucherDetailsVC
        obj.strVoucherId = myStringArr.last ?? ""
        obj.strType = "Rest"
        navigationController?.pushViewController(obj, animated: true)
        
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    
    
    // MARK: - Webservice
    
    func getRestaurantAPI() {
        Helper.shared.showHUD()
        let headers = ["Accept":"application/json"]
        
        var strCity = txtNameCity.text ?? ""
        if strCity.contains(" ") {
            
            strCity = strCity.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
        }
        print(strCity)
        NetworkManager.shared.webserviceCallGetRest(url: "\(URLs.get_restaurent)?search_name_city=\(strCity)&page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrRest += response.data!.data!
                self.arrSearchedRest = self.arrRest
                self.collectionview.reloadData()
                
                self.last_page = response.data!.last_page ?? 0
                var arrMarker:[GMSMarker] = []
                for i in 0..<(self.arrRest.count ?? 0) {
                    
                    let dict = self.arrRest[i]
                    let marker: GMSMarker = GMSMarker()
                    
                    marker.title = dict.restaurant_name
                    marker.snippet = dict.city
                    marker.position = CLLocationCoordinate2D(latitude: CLLocationSpeed(dict.latitude ?? "0.0")! , longitude: CLLocationSpeed(dict.longitude ?? "0.0")!)
                    marker.appearAnimation = .pop
                    marker.map = self.mapview
                    marker.accessibilityLabel = "\(i)"
                    arrMarker.append(marker)
                }
                
                var bounds = GMSCoordinateBounds()
                for marker in arrMarker
                {
                    bounds = bounds.includingCoordinate(marker.position)
                }
                let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
                self.mapview.animate(with: update)
                
                self.zoom = self.mapview.camera.zoom
            }
            SVProgressHUD.dismiss()
        }
    }

    @IBAction func btnBack(_ sender: Any) {
        if strFromVC == "LeftMenu" || strFromVC == "LeftMenu" {
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func btnPlus(_ sender: Any) {
        
        zoom = zoom + 1.0
        self.mapview.animate(toZoom: zoom)
    }
    
    @IBAction func btnMinus(_ sender: Any) {
        
        if zoom != 1.0 {
            zoom = zoom - 1
            self.mapview.animate(toZoom: zoom)
        }
    }
    @IBAction func btnRecenter(_ sender: Any) {
        self.mapview.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 10))
    }
   
    
    @IBAction func btnSearch(_ sender: Any) {
        
        if txtNameCity.text?.isEmpty ?? true
        {
            return
        }
        mapview.clear()
        self.arrRest.removeAll()
        self.arrSearchedRest?.removeAll()
        getRestaurantAPI()
    }
    
    
    
    @IBAction func btnScanQR(_ sender: Any) {
        
        
        if UserDefaults.standard.value(forKey: "isRestLogin") != nil && UserDefaults.standard.value(forKey: "isRestLogin") as! Bool == true {
            
            openScanner()
        } else {
            
            let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            obj.strFrom = "General"
            Helper.shared.strLoginType = "Rest"
            obj.delegae = self
            navigationController?.pushViewController(obj, animated: true)
        }
    }
    @IBAction func btnLogout(_ sender: Any) {
        if Helper.shared.strLoginType == "User" {
            
            Helper.shared.userLogout()
        } else if Helper.shared.strLoginType == "Rest" {
            
            Helper.shared.restaurantLogout()
        }
        viewLogout.isHidden = true
    }
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
    
    @IBAction func btnTabbar(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            isShowPopup = true
            self.viewDidLoad()
        } else if sender.tag == 2 {
            
            isShowPopup = false
        }
    }
}
extension FindRestaurantVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrSearchedRest?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SupportRestCell", for: indexPath) as! SupportRestCell
        let dict = arrSearchedRest?[indexPath.row]
        
        cell.img.sd_setImage(with: URL(string: dict?.restaurant_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.lblRestName.text = dict?.restaurant_name
        cell.lblCity.text = dict?.city
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RestDetailsVC") as! RestDetailsVC
        obj.intRestId = arrSearchedRest?[indexPath.row].restaurant_id
        if isShowPopup == true {
            obj.strSelectedType = "BuyBite"
        } else {
            obj.strSelectedType = "CreateVoucher"
        }
        
        navigationController?.pushViewController(obj, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionview.frame.width/2)-10, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if intCountArr != self.arrSearchedRest?.count {
            if (indexPath.row == (self.arrSearchedRest?.count ?? 0) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrSearchedRest?.count ?? 0
                    getRestaurantAPI()
                }
                
            }
        }
    }
    
    
}
extension FindRestaurantVC:AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            
            
            /////
//            var intVoucher_id:Int?
//            if let dict = try? JSONSerialization.jsonObject(with:Data(stringValue.utf8), options: []) as? [String:Any] {
//                guard let voucher_id = dict["voucher_id"] as? Int  else { return }
//                intVoucher_id = voucher_id
//                print(voucher_id)
//            }
            
            /////
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }

        dismiss(animated: true)
        
    }
    
    
    
}
extension FindRestaurantVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength = (textField.text!.count) + (string.count) - range.length

        if newLength == 0 {
            mapview.clear()
            self.arrRest.removeAll()
            self.arrSearchedRest?.removeAll()
            txtNameCity.text = ""
            getRestaurantAPI()
        }
        
        return true
    }
}

extension FindRestaurantVC:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let index:Int! = Int(marker.accessibilityLabel!)
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
        customInfoWindow.backView.shadowToView()
        customInfoWindow.lblName.text = arrSearchedRest?[index].restaurant_name
        customInfoWindow.lblType.text = arrSearchedRest?[index].restaurant_type
        customInfoWindow.img.sd_setImage(with: URL(string: arrSearchedRest?[index].restaurant_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))

        return customInfoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        let index:Int! = Int(marker.accessibilityLabel!)
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RestDetailsVC") as! RestDetailsVC
        obj.intRestId = arrSearchedRest?[index].restaurant_id
        if isShowPopup == true {
            obj.strSelectedType = "BuyBite"
        } else {
            obj.strSelectedType = "CreateVoucher"
        }
        
        navigationController?.pushViewController(obj, animated: true)
    }
}

//
//  RestDetailsVC.swift
//  BitesFunds
//
//  Created by Apple on 09/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import Stripe

class RestDetailsVC: UIViewController, isLogin {
    
    

    // MARK: - Variables
    var myPickerView: UIPickerView!
    var intTag = 1
    let arr1 = ["50(pay 40 USD)","100(pay 80 USD)","200(pay 160 USD)","Custom Amount"]
    let arr2 = ["50 BITE","100 BITE","200 BITE","Custom Amount"]
    var intRestId:Int!
    var strVoucherAmt:String?
    var strPay:String?
    var dict:GetRestDetailsData?
    var strSelectedType:String!
    var intSelectedRow:Int = 0
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var lblNoOfBites: UILabel!
    @IBOutlet weak var imgRest: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRestType: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtCustomAmount: UITextField!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var viewLogout: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getRestDetailsAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.value(forKey: "isUserLogin") != nil && UserDefaults.standard.value(forKey: "isUserLogin") as! Bool == true {
       
            if txtAmount.text == "" {
                return
            }
            if txtAmount.text == "Custom Amount" && txtCustomAmount.text == "" {
                return
            }
            if txtAmount.text == "Custom Amount" && txtCustomAmount.text != "" {
                
                strPay =  String(Int(Float(txtCustomAmount.text!)!*Float(0.8)))
                strVoucherAmt = txtCustomAmount.text
            }
            if intTag == 2 {
                createVoucherAPI()
            } else if intTag == 1 {
                //Stripe payment
                if txtAmount.text == "Custom Amount" && txtCustomAmount.text != "" {
                    
                    if Int("\(txtCustomAmount.text ?? "0")")! < 50 {
                        Toast.show(message: "Buy a minimum of 50 BITEs", controller: self)
                        return
                    }
                }
                
                let customView = Bundle.main.loadNibNamed("CardInfoView", owner: self, options: nil)?.first as! CardInfoView
                customView.btnSubmit.addTarget(self, action: #selector(btnSubmitPayment), for: .touchUpInside)
                customView.txtExpiry.delegate = self
                customView.txtCardNumber.delegate = self
                customView.lblPay.text = "Pay \(strPay ?? "") USD"
                customView.tag = 1001
                customView.frame = self.view.frame
                self.view.addSubview(customView)
            }
        }
    }
    func sendData() {
        Helper.shared.strLoginType = "Rest"
    }
    
    // MARK: - Function
    
    func setupUI() {
        
        if self.strSelectedType == "BuyBite" {
            
            btnConfirm.setTitle("Go to Payment", for: .normal)
            lblHeader.text = "How many BITE do you want?"
            
            lblNote.text = "Note : You pay 80 cents per BITE. Each BITE is worth USD 1 when redeemed in a restaurant."
            lblNoOfBites.text = "Choose the number of BITE to buy"
            heightView.constant = 0
            txtAmount.text = ""
            txtCustomAmount.text = ""
            intTag = 1
            //myPickerView.reloadAllComponents()
        } else if self.strSelectedType == "CreateVoucher" {
            
            btnConfirm.setTitle("Create Voucher", for: .normal)
            lblHeader.text = "Create Voucher"
            
            if dict?.percentage_you_want != nil && dict?.offer != nil {
                lblNote.text = "Note : 1 BITE = 1USD\n• Pay up to \(dict?.percentage_you_want ?? 0)% of the check in BITE\n• \(dict?.offer ?? "")"
            }
            if dict?.percentage_you_want == nil && dict?.offer != nil {
                lblNote.text = "Note : 1 BITE = 1USD\n• \(dict?.offer ?? "")"
            }
            if dict?.percentage_you_want != nil && dict?.offer == nil {
                lblNote.text = "Note : 1 BITE = 1USD\n• Pay up to \(dict?.percentage_you_want ?? 0)% of the check in BITE"
            }
            if dict?.percentage_you_want == nil && dict?.offer == nil {
                lblNote.text = "Note : 1 BITE = 1USD"
            }
            
                
            lblNoOfBites.text = "Number of BITE available in Wallet :\(dict?.bite_in_wallet ?? "")"
            
            heightView.constant = 0
            txtAmount.text = ""
            txtCustomAmount.text = ""
            intTag = 2
            //myPickerView.reloadAllComponents()
        }
        
        
        viewLogout.shadowToView()
        navigationController?.navigationBar.isHidden = true
        heightView.constant = 0
        
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        txtAmount.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtAmount.inputAccessoryView = toolBar
    }
    @objc func btnSubmitPayment() {
        
        let customView = self.view.viewWithTag(1001) as! CardInfoView
        
        //card parameters
        let stripeCardParams = STPCardParams()
        stripeCardParams.number = (customView.txtCardNumber.text ?? "").replacingOccurrences(of: " ", with: "")
        let expiryParameters = customView.txtExpiry.text?.components(separatedBy: "/")
        stripeCardParams.expMonth = UInt(expiryParameters?.first ?? "0") ?? 0
        stripeCardParams.expYear = UInt(expiryParameters?.last ?? "0") ?? 0
        stripeCardParams.cvc = customView.txtCVV.text
        
        //converting into token
        let config = STPPaymentConfiguration.shared()
        let stpApiClient = STPAPIClient.init(configuration: config)
        stpApiClient.createToken(withCard: stripeCardParams) { (token, error) in

        if error == nil {

        //Success
        DispatchQueue.main.async {
        print(token!.tokenId)
            self.buyBiteAPI(token: "\(token!.tokenId)")
        }

        } else {

        //failed
        print("Failed")
            Toast.show(message: "Failed", controller: self)
        }
        }
    }
    
    @objc func btnConfirmCreateVoucher() {
        self.view.viewWithTag(1002)?.removeFromSuperview()
        createVoucherAPI()
    }
    

    
    
    @objc func doneClick() {
        txtAmount.resignFirstResponder()
    }
    @objc func cancelClick() {
        txtAmount.resignFirstResponder()
    }
    
    // MARK: - Webservice
    
    func getRestDetailsAPI() {
        Helper.shared.showHUD()
        let headers = ["Accept":"application/json"]
        var url = URLs.restaurant_details + "\(intRestId ?? 0)"
        
        if UserDefaults.standard.value(forKey: "isUserLogin") != nil && UserDefaults.standard.value(forKey: "isUserLogin") as! Bool == true {
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            
            url = "\(url)&login_user_id=\(userId)"
        }
        print(url)
        NetworkManager.shared.webserviceCallGetRestDetails(url: url ,headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.dict = response.data
                self.lblCity.text = self.dict?.city
                self.lblTitle.text = self.dict?.restaurant_name
                self.lblDesc.text = self.dict?.description
                self.lblRestType.text = self.dict?.restaurant_type
                self.imgRest.sd_setImage(with: URL(string: self.dict?.restaurant_image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
                SVProgressHUD.dismiss()
                self.setupUI()
            } else {
                SVProgressHUD.dismiss()
            }
        }
        
    }

    func createVoucherAPI() {
        Helper.shared.showHUD()
        
        let params = ["restaurant_id":"\(intRestId ?? 0)",
            "voucher_amount":strVoucherAmt ?? ""]
        
         
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(UserDefaults.standard.value(forKey: "api_token") ?? "")"]
        NetworkManager.shared.webserviceCallCommon(url: URLs.user_create_voucher, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "ThankVC") as! ThankVC
                self.navigationController?.pushViewController(obj, animated: false)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    func buyBiteAPI(token:String) {
        Helper.shared.showHUD()
        
        let params = ["restaurant_id":"\(intRestId ?? 0)",
            "bite_payment_amount":strPay ?? "",
            "stripe_token":token]
        
        let headers = ["Accept":"application/json",
        "Authorization":"Bearer \(UserDefaults.standard.value(forKey: "api_token") ?? "")"]
        NetworkManager.shared.webserviceCallBuyBite(url: URLs.buy_bite, parameters: params, headers:headers) { (response) in
            self.view.viewWithTag(1001)?.removeFromSuperview()
            if response.ResponseCode == 200 {
                
                let obj = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "PaymentSuccessVC") as! PaymentSuccessVC
                obj.transaction_amount = "\(response.data?.transaction_amount ?? "0")"
                obj.number = response.data?.transaction_id
                self.navigationController?.pushViewController(obj, animated: false)
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }

    // MARK: - IBAction
    
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCreateVoucher(_ sender: Any) {
        if txtAmount.text == "" {
            return
        }
        if txtAmount.text == "Custom Amount" && txtCustomAmount.text == "" {
            return
        }
        if txtAmount.text == "Custom Amount" && txtCustomAmount.text != "" {
            
            
            strPay =  String(Int(Float(txtCustomAmount.text!)!*Float(0.8)))
            strVoucherAmt = txtCustomAmount.text
        }
        //createVoucherAPI()
        
        if UserDefaults.standard.value(forKey: "isUserLogin") != nil && UserDefaults.standard.value(forKey: "isUserLogin") as! Bool == true {
            
            if intTag == 2 {
                
                
                let customView = Bundle.main.loadNibNamed("ConfirmVoucherView", owner: self, options: nil)?.first as! ConfirmVoucherView
                customView.btnConfrim.addTarget(self, action: #selector(btnConfirmCreateVoucher), for: .touchUpInside)
                customView.lblDesc.text = "Please confirm that you would like to create a voucher of a value of \(strVoucherAmt ?? "") BITE to be spent at \(self.dict?.restaurant_name ?? "")"
                customView.tag = 1002
                customView.frame = self.view.frame
                self.view.addSubview(customView)
            } else if intTag == 1 {
                //Stripe payment
                if txtAmount.text == "Custom Amount" && txtCustomAmount.text != "" {
                    
                    if Int("\(txtCustomAmount.text ?? "0")")! < 50 {
                        Toast.show(message: "Buy a minimum of 50 BITEs", controller: self)
                        return
                    }
                }
                let customView = Bundle.main.loadNibNamed("CardInfoView", owner: self, options: nil)?.first as! CardInfoView
                customView.btnSubmit.addTarget(self, action: #selector(btnSubmitPayment), for: .touchUpInside)
                customView.txtExpiry.delegate = self
                customView.txtCardNumber.delegate = self
                customView.lblPay.text = "Pay \(strPay ?? "") USD"
                customView.tag = 1001
                customView.frame = self.view.frame
                self.view.addSubview(customView)
            }
            
        } else {
            
            let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            obj.strFrom = "General"
            Helper.shared.strLoginType = "User"
            obj.delegae = self
            navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    @IBAction func btnDropdown(_ sender: Any) {
        
        
    }
    @IBAction func btnLogout(_ sender: Any) {
        if Helper.shared.strLoginType == "User" {
            
            Helper.shared.userLogout()
        } else if Helper.shared.strLoginType == "Rest" {
            
            Helper.shared.restaurantLogout()
        }
        viewLogout.isHidden = true
    }
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
}



extension RestDetailsVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if intTag == 1 {
            return arr1.count
        }
        if intTag == 2 {
            return arr2.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if intTag == 1 {
            return arr1[row]
        }
        if intTag == 2 {
            return arr2[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if row == 3 {
            heightView.constant = 87
        } else {
            heightView.constant = 0
        }
        if intTag == 1 {
            self.txtAmount.text = arr1[row]
            
            if row == 0 {
                strVoucherAmt = "50"
                strPay = "40"
            } else if row == 1 {
                strVoucherAmt = "100"
                strPay = "80"
            } else if row == 2 {
                strVoucherAmt = "200"
                strPay = "160"
            } else {
                strVoucherAmt = ""
                strPay = ""
            }
        }
        if intTag == 2 {
            
            self.txtAmount.text = arr2[row]
            
            if row == 0 {
                strVoucherAmt = "50"
            } else if row == 1 {
                strVoucherAmt = "100"
            } else if row == 2 {
                strVoucherAmt = "200"
            } else if row == 3 {
                strVoucherAmt = ""
            }
            
        }
        intSelectedRow = row
        print(strVoucherAmt)
    }
    
    func expDateValidation(dateStr:String) {

        let currentYear = Calendar.current.component(.year, from: Date()) % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)

        let enteredYear = Int(dateStr.suffix(2)) ?? 0 // get last two digit from entered string as year
        let enteredMonth = Int(dateStr.prefix(2)) ?? 0 // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user

        if enteredYear > currentYear {
            if (1 ... 12).contains(enteredMonth) {
                print("Entered Date Is Right")
            } else {
                print("Entered Date Is Wrong")
            }
        } else if currentYear == enteredYear {
            if enteredMonth >= currentMonth {
                if (1 ... 12).contains(enteredMonth) {
                   print("Entered Date Is Right")
                } else {
                   print("Entered Date Is Wrong")
                }
            } else {
                print("Entered Date Is Wrong")
            }
        } else {
           print("Entered Date Is Wrong")
        }

    }
}

extension RestDetailsVC:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtAmount {
            
            self.myPickerView.selectRow(intSelectedRow, inComponent: 0, animated: true)
            self.pickerView(myPickerView, didSelectRow: intSelectedRow, inComponent: 0)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == (self.view.viewWithTag(1001) as! CardInfoView).txtCardNumber
        {
            guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
            
            if currentText.count<20 {
                textField.text = currentText.grouping(every: 4, with: " ")
                return false
            } else {
                return false
            }
            
        }
        
        //////////////////
        
        if textField == (self.view.viewWithTag(1001) as! CardInfoView).txtExpiry {
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            let updatedText = oldText.replacingCharacters(in: r, with: string)

            if string == "" {
                if updatedText.count == 2 {
                    textField.text = "\(updatedText.prefix(1))"
                    return false
                }
            } else if updatedText.count == 1 {
                if updatedText > "1" {
                    return false
                }
            } else if updatedText.count == 2 {
                if updatedText <= "12" { //Prevent user to not enter month more than 12
                    textField.text = "\(updatedText)/" //This will add "/" when user enters 2nd digit of month
                }
                return false
            } else if updatedText.count == 5 {
                self.expDateValidation(dateStr: updatedText)
            } else if updatedText.count > 5 {
                return false
            }

            return true
        }
        return true
    }
}
extension String {
    func grouping(every groupSize: String.IndexDistance, with separator: Character) -> String {
       let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
       return String(cleanedUpCopy.enumerated().map() {
            $0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
       }.joined().dropFirst())
    }
}

//
//  HowWorkVC.swift
//  BitesFunds
//
//  Created by Apple on 10/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class HowWorkVC: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    // MARK: - Variables
    
    var strHeader:String?
    var strUrl:String?
    
    // MARK: - IBOutlet
    @IBOutlet weak var webview: WKWebView!
    var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblHeader: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    
    
    
    // MARK: - Function
    
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        lblHeader.text = strHeader!
        
        webview.translatesAutoresizingMaskIntoConstraints = false
        webview.isUserInteractionEnabled = true
        webview.navigationDelegate = self
        guard let url = URL(string: strUrl!) else { return }
        let request = URLRequest(url: url)
        webview.load(request)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        Helper.shared.showHUD()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
    }
}

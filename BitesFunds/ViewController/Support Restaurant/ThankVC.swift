//
//  ThankVC.swift
//  BitesFunds
//
//  Created by Apple on 10/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import MMDrawerController

class ThankVC: UIViewController {

    
    @IBOutlet weak var view_back: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.navigationBar.isHidden = true
        view_back.shadowToView()
    }
    

    // MARK: - IBAction
    
    @IBAction func btnBackToHome(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate


        if Helper.shared.strLoginType == "User" {
            let centerViewController = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "UserDashboardVC") as! UserDashboardVC
            let leftViewController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "GeneralMenuVC") as! GeneralMenuVC

            let leftSideNav = UINavigationController(rootViewController: leftViewController)
            leftSideNav.navigationBar.isHidden = true
            let centerNav = UINavigationController(rootViewController: centerViewController)
            centerNav.navigationBar.isHidden = true
            appDelegate.centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)

            appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
            appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapCenterView
            appDelegate.window?.rootViewController = appDelegate.centerContainer
            appDelegate.centerContainer?.maximumLeftDrawerWidth = 350
            
        } else if Helper.shared.strLoginType == "Rest" {
            let centerViewController = UIStoryboard(name: "RestaurantDashboard", bundle: nil).instantiateViewController(withIdentifier: "RestDashboardVC") as! RestDashboardVC
            let leftViewController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "GeneralMenuVC") as! GeneralMenuVC

            let leftSideNav = UINavigationController(rootViewController: leftViewController)
            leftSideNav.navigationBar.isHidden = true
            let centerNav = UINavigationController(rootViewController: centerViewController)
            centerNav.navigationBar.isHidden = true
            appDelegate.centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)

            appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
            appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapCenterView
            appDelegate.window?.rootViewController = appDelegate.centerContainer
            appDelegate.centerContainer?.maximumLeftDrawerWidth = 350
        }
        //self.performSegue(withIdentifier: "segueUserDashboard", sender: self)
    }
    @IBAction func btnBack(_ sender: Any) {
        
        
    }
    

    

}

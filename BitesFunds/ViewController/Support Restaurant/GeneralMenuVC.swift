//
//  GeneralMenuVC.swift
//  BitesFunds
//
//  Created by Apple on 12/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import MMDrawerController


class GeneralMenuVC: UIViewController {
    
    //MARK: - Variables
    
    var arrItems2:[String] = ["Home","Dashboard","My Profile","Buy BITE","Create Voucher","Redeemed Voucher List","About","Contact Us","Transaction History"]
    var arrImages2:[String] = ["home","dashboard","profile","buy_bites","create_voucher","redeemed_voucher_list","about","contact","transaction_history"]
    var arrItems3:[String] = ["Home","Dashboard","My Profile","Fund Raised","Redeemed Voucher List","About","Contact Us","Transaction History"]
    var arrImages3:[String] = ["home","dashboard","profile","fund_raised","redeemed_voucher_list","about","contact","transaction_history"]
    
    //MARK: - IBOutlet
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var viewBack:UIView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var lblProfileName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        setupUI()
        tableview.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")

    }
    override func viewWillAppear(_ animated: Bool) {
        if Helper.shared.strLoginType == "User" || Helper.shared.strLoginType == "Rest" {
            viewBack.isHidden = false
            heightView.constant = 80
        }
        tableview.reloadData()
        
        if Helper.shared.strLoginType == "User" {
            
            if UserDefaults.standard.value(forKey: "isUserLogin") != nil && UserDefaults.standard.value(forKey: "isUserLogin") as! Bool == true {
                
                lblProfileName.text = "Hello, \(Helper.shared.user_name)"
                self.imgProfile.sd_setImage(with: URL(string: Helper.shared.user_profile_pic ), placeholderImage: #imageLiteral(resourceName: "placeholder"))
                getPaidBightBoughtAPI()
            }
        }
        if Helper.shared.strLoginType == "Rest" {
            
            if UserDefaults.standard.value(forKey: "isRestLogin") != nil && UserDefaults.standard.value(forKey: "isRestLogin") as! Bool == true {
                
                lblProfileName.text = Helper.shared.restaurant_name
                self.imgProfile.sd_setImage(with: URL(string: Helper.shared.restaurant_profile_pic), placeholderImage: #imageLiteral(resourceName: "placeholder"))
                getTotalFundRaisedAPI()
                

            }
        }
        
        
    }
    //MARK: - Function
    
    func setupUI() {
        
        tableview.rowHeight = 60
    }
    
    // MARK: - Webservice
    func getPaidBightBoughtAPI() {
        
        let headers = ["Accept":"application/json",
        "Authorization":"Bearer \(Helper.shared.user_token)"]
        NetworkManager.shared.webserviceCallPaidBiteBought(url: URLs.get_usd_paid_bite_bought, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblFirst.text = "\(response.data?.usd_paid ?? "") USD Paid"
                self.lblSecond.text = "\(response.data?.bite_bought ?? "") BITE Bought"
                
            }
        }
    }
    func getTotalFundRaisedAPI() {
        
        let headers = ["Accept":"application/json",
        "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        NetworkManager.shared.webserviceCallTotalFundRaised(url: URLs.restaurant_get_total_funds_raised, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblFirst.text = "\(response.data?.total_funds_raised ?? "") USD Total Fund Raised"
                self.lblSecond.text = ""
                if Helper.shared.restaurant_profile_pic == "" || Helper.shared.restaurant_description == "" {
                    let indexPath = IndexPath(row: 2, section: 0)
                    self.tableview.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                    self.tableview.delegate?.tableView!(self.tableview, didSelectRowAt: indexPath)
                }
            }
        }
    }
    
    //MARK: - IBAction
    
    @IBAction func btnUserLogin(_ sender: Any) {
        Helper.shared.strLoginType = "User"
        
        let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "SupportRestaurantVC") as! SupportRestaurantVC
        let navController = UINavigationController(rootViewController: centerViewController)
        navController.navigationController?.navigationBar.isHidden = true
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.centerViewController = navController
        appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: false, completion: nil)
    }
    
    @IBAction func btnRestLogin(_ sender: Any) {
        Helper.shared.strLoginType = "Rest"
        
        let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "SupportRestaurantVC") as! SupportRestaurantVC
        let navController = UINavigationController(rootViewController: centerViewController)
        navController.navigationController?.navigationBar.isHidden = true
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.centerViewController = navController
        appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: false, completion: nil)
    }
    
    
}

extension GeneralMenuVC: UITableViewDataSource, UITableViewDelegate {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if Helper.shared.strLoginType == "User" {
            return arrItems2.count
        }else if Helper.shared.strLoginType == "Rest" {
            return arrItems3.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        if Helper.shared.strLoginType == "User" {
            cell.lblTitle.text = arrItems2[indexPath.row]
            cell.imgIcon.image = UIImage(named: arrImages2[indexPath.row])
        }else if Helper.shared.strLoginType == "Rest" {
            cell.lblTitle.text = arrItems3[indexPath.row]
            cell.imgIcon.image = UIImage(named: arrImages3[indexPath.row])
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if Helper.shared.strLoginType == "User" {
            if indexPath.row == 0 {
                
                let centerViewController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "SupportRestaurantVC") as! SupportRestaurantVC
                let navController = UINavigationController(rootViewController: centerViewController)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 1 {
                
                let centerViewController = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "UserDashboardVC") as! UserDashboardVC
                let navController = UINavigationController(rootViewController: centerViewController)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 2 {
                
                let obj = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 3 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "FindRestaurantVC") as! FindRestaurantVC
                obj.strFromVC = "LeftMenu"
                obj.isShowPopup = true
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 4 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "FindRestaurantVC") as! FindRestaurantVC
                obj.strFromVC = "LeftMenu"
                obj.isShowPopup = false
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 5 {
                
                let obj = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "RedeemedVoucherListVC") as! RedeemedVoucherListVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            
            if indexPath.row == 6 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HowWorkVC") as! HowWorkVC
                obj.strHeader = "ABOUT US"
                obj.strUrl = URLs.baseurl + "about"
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 7 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
                //obj.strHeader = "CONTACT US"
                //obj.strUrl = "http://chessmafia.com/php/BitesAndFunds/api/contact"
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 8 {
                
                let obj = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "TransactionHistoryVC") as! TransactionHistoryVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
        } else if Helper.shared.strLoginType == "Rest" {
            if indexPath.row == 0 {
                
                let centerViewController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "SupportRestaurantVC") as! SupportRestaurantVC
                let navController = UINavigationController(rootViewController: centerViewController)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 1 {
                
                let centerViewController = UIStoryboard(name: "RestaurantDashboard", bundle: nil).instantiateViewController(withIdentifier: "RestDashboardVC") as! RestDashboardVC
                let navController = UINavigationController(rootViewController: centerViewController)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 2 {
                
                let obj = UIStoryboard(name: "RestaurantDashboard", bundle: nil).instantiateViewController(withIdentifier: "RestProfileVC") as! RestProfileVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 3 {
                
                let obj = UIStoryboard(name: "RestaurantDashboard", bundle: nil).instantiateViewController(withIdentifier: "FundRaisedVC") as! FundRaisedVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 4 {
                
                let obj = UIStoryboard(name: "RestaurantDashboard", bundle: nil).instantiateViewController(withIdentifier: "RestRedeemedListVC") as! RestRedeemedListVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            
            if indexPath.row == 5 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HowWorkVC") as! HowWorkVC
                obj.strHeader = "ABOUT US"
                obj.strUrl = URLs.baseurl + "about"
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 6 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
                //obj.strHeader = "CONTACT US"
                //obj.strUrl = "http://chessmafia.com/php/BitesAndFunds/api/contact"
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 7 {
                
                let obj = UIStoryboard(name: "RestaurantDashboard", bundle: nil).instantiateViewController(withIdentifier: "RestTransactionHistoryVC") as! RestTransactionHistoryVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
        } else if Helper.shared.strLoginType == "General" {
            if indexPath.row == 0 {
                
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "SupportRestaurantVC") as! SupportRestaurantVC
                let navController = UINavigationController(rootViewController: centerViewController)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 1 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "FindRestaurantVC") as! FindRestaurantVC
                obj.strFromVC = "LeftMenuBuyBite"
                obj.isShowPopup = true
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 2 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
                //obj.strHeader = "ABOUT US"
                //obj.strUrl = "http://chessmafia.com/php/BitesAndFunds/api/about"
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 3 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "HowWorkVC") as! HowWorkVC
                obj.strHeader = "CONTACT US"
                obj.strUrl = "http://chessmafia.com/php/BitesAndFunds/api/contact"
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
        }
    }
    
}

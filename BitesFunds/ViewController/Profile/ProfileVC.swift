//
//  ProfileVC.swift
//  BitesFunds
//
//  Created by Apple on 08/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import MobileCoreServices

class ProfileVC: UIViewController {

    // MARK: - Variables
    
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var view1:UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var txtFName: FormTextField!
    @IBOutlet weak var txtLName
    : FormTextField!
    @IBOutlet weak var txtEmail: FormTextField!
    @IBOutlet weak var txtPhone: FormTextField!
    @IBOutlet weak var txtOldPassword: FormTextField!
    @IBOutlet weak var txtNewPassword: FormTextField!
    @IBOutlet weak var txtCongPassword: FormTextField!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnEditPic: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getProfileAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        view1.shadowToView()
        view2.shadowToView()
        btnEditProfile.layer.borderWidth = 1
        btnEditProfile.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        btnChangePassword.layer.borderWidth = 1
        btnChangePassword.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))

        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))

        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            actionSheet.popoverPresentationController?.sourceView = btnEditPic
            actionSheet.popoverPresentationController?.sourceRect = btnEditPic.bounds
            actionSheet.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(actionSheet, animated: true, completion: nil)

    }
    
    func camera()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerController.SourceType.camera

        self.present(myPickerController, animated: true, completion: nil)

    }

    func photoLibrary()
    {

        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.mediaTypes = [(kUTTypeImage as String)]
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary

        self.present(myPickerController, animated: true, completion: nil)

    }
    
    //MARK: - Webservice
    func getProfileAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.user_token)"]
        NetworkManager.shared.webserviceCallUserGetProfile(url: URLs.user_get_profile, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtFName.text = response.data?.first_name
                self.txtLName.text = response.data?.last_name
                self.txtPhone.text = response.data?.phone_no
                self.txtEmail.text = response.data?.email
                self.imgProfile.sd_setImage(with: URL(string: response.data?.profile_pic ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    func updateProfileAPI() {
        Helper.shared.showHUD()
        
        let imageData = (imgProfile.image?.jpegData(compressionQuality: 0.6))!
        
        let params = ["first_name":txtFName.text,
                      "last_name":txtLName.text,
                      "email":txtEmail.text,
                      "phone_no":txtPhone.text] as! [String:String]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.user_token)"]
        NetworkManager.shared.webserviceCallEditProfile(url: URLs.user_update_profile, parameters: params, imgData: imageData, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    func changePasswordAPI() {
        Helper.shared.showHUD()
        
        let params = ["old_password":txtOldPassword.text,
                      "password":txtNewPassword.text] as! [String:String]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.user_token)"]
        NetworkManager.shared.webserviceCallCommon(url: URLs.user_change_password, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtOldPassword.text = ""
                self.txtNewPassword.text = ""
                self.txtCongPassword.text = ""
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    

    // MARK: - IBAction
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnEditProfile(_ sender: UIButton) {
        
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        sender.backgroundColor = #colorLiteral(red: 0.5044823289, green: 0.1835186183, blue: 0.5145391226, alpha: 1)
        btnChangePassword.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnChangePassword.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        view2.isHidden = true
        view1.isHidden = false
        
        txtFName.isUserInteractionEnabled = true
        txtLName.isUserInteractionEnabled = true
        txtEmail.isUserInteractionEnabled = true
        txtPhone.isUserInteractionEnabled = true
    }
    
    @IBAction func btnChangePassword(_ sender: UIButton) {
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        sender.backgroundColor = #colorLiteral(red: 0.5044823289, green: 0.1835186183, blue: 0.5145391226, alpha: 1)
        btnEditProfile.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnEditProfile.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        view2.isHidden = false
        view1.isHidden = true
    }
    
    @IBAction func btnUpdateChanges(_ sender: Any) {
        
        if txtFName.text?.isEmpty ?? true {
            Toast.show(message: Message.enterFName, controller: self)
            return
        }
        if txtLName.text?.isEmpty ?? true {
            Toast.show(message: Message.enterLName, controller: self)
            return
        }
        if txtEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !(txtEmail.text)!.isValidEmail() {
                Toast.show(message: Message.enterValidEmail, controller: self)
                return
            }
        }
        if txtPhone.text?.isEmpty ?? true {
            Toast.show(message: Message.enterNumber, controller: self)
            return
        }
        updateProfileAPI()
    }
    @IBAction func btnChangePass(_ sender: Any) {
        
        if txtOldPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if txtNewPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if txtCongPassword.text != txtNewPassword.text {
            Toast.show(message: Message.passwordUnmatched, controller: self)
            return
        }
        
        changePasswordAPI()
    }
    @IBAction func btnChangeImage(_ sender: Any) {
        
        showActionSheet()
    }
}
extension ProfileVC:UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        print("Image Selected")
        imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
}

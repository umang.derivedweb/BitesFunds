//
//  RestDashboardVC.swift
//  BitesFunds
//
//  Created by Apple on 10/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import MMDrawerController
import AVFoundation

class RestDashboardVC: UIViewController, VoucherDetailsDelegate {
    
    

    // MARK: - Variables
    var arrVoucher:[VoucherListData] = []
    var arrSearchedVoucher:[VoucherListData]?
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var lblWalletBite: UILabel!
    @IBOutlet weak var lblActiveVoucher: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblBiteinVoucher: UILabel!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        dashboardAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        viewLogout.shadowToView()
        table.rowHeight = 111
        navigationController?.navigationBar.isHidden = true
        
        if Helper.shared.restaurant_profile_pic == "" || Helper.shared.restaurant_description == "" {
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.toggle(.left, animated: false, completion: nil)
        }
        
    }
    func openScanner() {
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)

        captureSession.startRunning()
        
    }
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    func found(code: String) {
        print(code)
        let myStringArr = code.components(separatedBy: "/")
        
        let obj = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "VoucherDetailsVC") as! VoucherDetailsVC
        obj.strVoucherId = myStringArr.last ?? ""
        obj.strType = "Rest"
        obj.delegate = self
        navigationController?.pushViewController(obj, animated: true)
        
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    func voucherDetailsFailed(msg: String) {
        Toast.show(message: msg, controller: self)
    }
    //MARK: - Webservice
    func dashboardAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        
        NetworkManager.shared.webserviceCallUserDashboard(url: "\(URLs.restaurant_dashboard)?page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.last_page = response.data?.vouchers?.last_page ?? 0
                self.lblWalletBite.text = response.data?.bite_in_wallet
                self.lblBiteinVoucher.text = response.data?.total_active_voucher_bite
                self.lblActiveVoucher.text = "BITE in \(response.data?.active_voucher ?? "0") active vouchers"
                self.arrVoucher += (response.data?.vouchers?.data ?? [])
                self.arrSearchedVoucher = self.arrVoucher
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    
    // MARK: - IBAction
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
    @IBAction func btnLogout(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: false)
        Helper.shared.restaurantLogout()
    }
    @IBAction func btnVoucherHistory(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RestRedeemedListVC") as! RestRedeemedListVC
        obj.isFrom = "Dashboard"
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnSearch(_ sender: Any) {
        
        if txtSearch.text?.isEmpty ?? true
        {
            return
        }
        
        arrSearchedVoucher = arrVoucher.filter { ($0.name?.localizedCaseInsensitiveContains(txtSearch?.text ?? ""))! }
        
        self.table.reloadData()
    }
    
    @IBAction func btnScanQR(_ sender: Any) {
        
        openScanner()
    }
    
    @IBAction func btnBiteInVoucher(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RestTransactionHistoryVC") as! RestTransactionHistoryVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
extension RestDashboardVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchedVoucher?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VoucherListCell") as! VoucherListCell
        
        let dict = arrSearchedVoucher?[indexPath.row]
        cell.lblRestName.text = dict?.name
        cell.lblBite.text = dict?.bite
        cell.lblDate.text = dict?.date
        cell.lblVoucherCode.text = dict?.voucher_code
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "VoucherDetailsVC") as! VoucherDetailsVC
        obj.strVoucherId = "\(arrSearchedVoucher?[indexPath.row].voucher_id ?? 0)"
        obj.strType = "Rest"
        obj.delegate = self
        navigationController?.pushViewController(obj, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrSearchedVoucher?.count {
            if (indexPath.row == (self.arrSearchedVoucher?.count ?? 0) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrSearchedVoucher?.count ?? 0
                    dashboardAPI()
                }
                
            }
        }
    }
}

extension RestDashboardVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength = (textField.text!.count) + (string.count) - range.length

        if newLength == 0 {
            self.arrSearchedVoucher = self.arrVoucher
            table.reloadData()
        }
        
        return true
    }
}
extension RestDashboardVC:AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }

        dismiss(animated: true)
        
    }
    
    
    
}

//
//  RestProfileVC.swift
//  BitesFunds
//
//  Created by Apple on 12/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import MobileCoreServices

class RestProfileVC: UIViewController {
    
    
    //MARK: Variables
    
    var strCityId:String?
    var myPickerView: UIPickerView!
    var arrRestType:[RestTypeData]?
    var strRestTypeId:String?
    var arrPercentage = ["50","60","70","80","90","100"]
    var intTag:Int?
    var arrCities:[CitiesListData]?
    var arrStates:[StateData]?
    var strSelectedCityId:String?
    var strState_id:String?
    
    
    //MARK: IBOutlet
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnSelectImage: UIButton!
    @IBOutlet weak var txtNewPassword: FormTextField!
    @IBOutlet weak var txtConfPassword: FormTextField!
    @IBOutlet weak var txtOldPassword: FormTextField!
    @IBOutlet weak var txtview: UITextView!
    @IBOutlet weak var txtRestType: FormTextField!
    @IBOutlet weak var txEmail: FormTextField!
    @IBOutlet weak var txtRestName: FormTextField!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var txtPercentage: FormTextField!
    @IBOutlet weak var txtOffer: FormTextField!
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var view1:UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var txtRestState: FormTextField!
    @IBOutlet weak var txtRestCity: FormTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.isHidden = true
        
        getProfileAPI()
        getRestTypeAPI()
        
        getStatesAPI()
        setupUI()
    }
    
    //MARK: - Function
    
    func setupUI() {
        
        view1.shadowToView()
        view2.shadowToView()
        txtview.layer.cornerRadius = 5
        txtview.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        txtview.layer.borderWidth = 1
        btnEditProfile.layer.borderWidth = 1
        btnEditProfile.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        btnChangePassword.layer.borderWidth = 1
        btnChangePassword.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        txtRestType.inputView = self.myPickerView
        txtPercentage.inputView = self.myPickerView
        txtRestState.inputView = self.myPickerView
        txtRestCity.inputView = self.myPickerView
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtRestType.inputAccessoryView = toolBar
        txtPercentage.inputAccessoryView = toolBar
        txtRestCity.inputAccessoryView = toolBar
        txtRestState.inputAccessoryView = toolBar
        
        if Helper.shared.restaurant_profile_pic == "" || Helper.shared.restaurant_description == "" {
            
            let customView = Bundle.main.loadNibNamed("RestProfileView", owner: self, options: nil)?.first as! RestProfileView
            customView.frame = self.view.frame
            self.view.addSubview(customView)
        }
    }
    @objc func doneClick() {
        if intTag == 3 {
            getCitiesAPI()
        }
        (self.view.viewWithTag(intTag!) as! UITextField).resignFirstResponder()
    }
    @objc func cancelClick() {
        (self.view.viewWithTag(intTag!) as! UITextField).resignFirstResponder()
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            actionSheet.popoverPresentationController?.sourceView = btnSelectImage
            actionSheet.popoverPresentationController?.sourceRect = btnSelectImage.bounds
            actionSheet.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func camera()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        
        self.present(myPickerController, animated: true, completion: nil)
        
    }
    
    func photoLibrary()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.mediaTypes = [(kUTTypeImage as String)]
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    //MARK: - Webservice
    func getProfileAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        NetworkManager.shared.webserviceCallRestGetProfile(url: URLs.restaurant_get_profile, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtRestName.text = response.data?.restaurant_name
                self.txtOffer.text = response.data?.offer
                self.txtview.text = response.data?.description
                self.txEmail.text = response.data?.email
                self.txtPercentage.text = response.data?.percentage_you_want
                
                self.txtRestType.text = response.data?.restaurant_type
                self.txtRestState.text = response.data?.state_name
                self.txtRestCity.text = response.data?.city
                self.strSelectedCityId = response.data?.city_id
                self.strState_id = response.data?.state_id
                self.strRestTypeId = response.data?.restaurant_type_id
                self.getCitiesAPI()
                
                self.imgProfile.sd_setImage(with: URL(string: response.data?.restaurant_image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
                
                Helper.shared.restaurant_profile_pic = response.data?.restaurant_image ?? ""
                Helper.shared.restaurant_description = response.data?.description ?? ""
                
                UserDefaults.standard.set(response.data?.restaurant_image, forKey: "restaurant_image")
                UserDefaults.standard.set(response.data?.description, forKey: "restaurant_description")
                UserDefaults.standard.synchronize()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    func getRestTypeAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        NetworkManager.shared.webserviceCallRestType(url: URLs.get_restaurant_type, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrRestType = response.data
                self.myPickerView.reloadAllComponents()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    func getCitiesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCities(url: URLs.get_cities+"?state_id=\(strState_id ?? "")", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCities = response.data?.data
                self.myPickerView.reloadAllComponents()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    func getStatesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetStates(url: URLs.get_states, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrStates = response.data?.data
                self.myPickerView.reloadAllComponents()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    func changePasswordAPI() {
        Helper.shared.showHUD()
        
        let params = ["old_password":txtOldPassword.text,
                      "password":txtNewPassword.text] as! [String:String]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        NetworkManager.shared.webserviceCallCommon(url: URLs.restaurant_change_password, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtOldPassword.text = ""
                self.txtNewPassword.text = ""
                self.txtConfPassword.text = ""
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    func updateProfileAPI() {
        Helper.shared.showHUD()
        
        let imageData = (imgProfile.image?.jpegData(compressionQuality: 0.6))!
        
        let params = ["restaurant_name":txtRestName.text ?? "",
                      "restaurant_type":strRestTypeId ?? "",
                      "email":txEmail.text ?? "",
                      "city_id":strSelectedCityId ?? "",
                      "description":txtview.text ?? "",
                      "offer":txtOffer.text ?? "",
                      "percentage_you_want":txtPercentage.text ?? ""]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        NetworkManager.shared.webserviceCallRestEditProfile(url: URLs.restaurant_update_profile, parameters: params, imgData: imageData, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.getProfileAPI()
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    //MARK: - IBAction
    
    @IBAction func btnChangePass(_ sender: Any) {
        
        if txtOldPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if txtNewPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if txtConfPassword.text != txtNewPassword.text {
            Toast.show(message: Message.passwordUnmatched, controller: self)
            return
        }
        
        changePasswordAPI()
    }
    @IBAction func btnMenu(_ sender: Any) {
        if Helper.shared.restaurant_profile_pic == "" || Helper.shared.restaurant_description == "" {
            return
        }
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    
    @IBAction func btnEditProfile(_ sender: UIButton) {
        
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        sender.backgroundColor = #colorLiteral(red: 0.5044823289, green: 0.1835186183, blue: 0.5145391226, alpha: 1)
        btnChangePassword.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnChangePassword.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        view2.isHidden = true
        view1.isHidden = false
        
        txtview.isUserInteractionEnabled = true
        txtRestType.isUserInteractionEnabled = true
        txtRestCity.isUserInteractionEnabled = true
        txtRestState.isUserInteractionEnabled = true
        txEmail.isUserInteractionEnabled = true
        txtRestName.isUserInteractionEnabled = true
        txtPercentage.isUserInteractionEnabled = true
        txtOffer.isUserInteractionEnabled = true
    }
    
    @IBAction func btnChangePassword(_ sender: UIButton) {
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        sender.backgroundColor = #colorLiteral(red: 0.5044823289, green: 0.1835186183, blue: 0.5145391226, alpha: 1)
        btnEditProfile.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnEditProfile.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        view2.isHidden = false
        view1.isHidden = true
    }
    
    @IBAction func btnEditPic(_ sender: Any) {
        showActionSheet()
    }
    @IBAction func btnUpdateChanges(_ sender: Any) {
        if txtRestName.text?.isEmpty ?? true {
            Toast.show(message: Message.enterFName, controller: self)
            return
        }
        if txtview.text?.isEmpty ?? true {
            Toast.show(message: Message.enterDesc, controller: self)
            return
        }
        if txEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !(txEmail.text)!.isValidEmail() {
                Toast.show(message: Message.enterValidEmail, controller: self)
                return
            }
        }
        
        updateProfileAPI()
    }
}
extension RestProfileVC:UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        print("Image Selected")
        imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
extension RestProfileVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if intTag == 1 {
            return arrRestType?.count ??  0
        }
        if intTag == 2 {
            return arrPercentage.count
        }
        if intTag == 3 {
            return arrStates?.count ?? 0
        }
        if intTag == 4 {
            return arrCities?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if intTag == 1 {
            return arrRestType?[row].restaurant_type
        }
        if intTag == 2 {
            return arrPercentage[row]
        }
        if intTag == 3 {
            return arrStates?[row].state_name
        }
        if intTag == 4 {
            return arrCities?[row].city
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if intTag == 1 {
            self.txtRestType.text = arrRestType?[row].restaurant_type
            strRestTypeId = "\(arrRestType?[row].restaurant_type_id ?? 0)"
        }
        if intTag == 2 {
            self.txtPercentage.text = arrPercentage[row]
        }
        if intTag == 3 {
            self.txtRestState.text = arrStates?[row].state_name
            strState_id = "\(arrStates?[row].state_id ?? "")"
        }
        if intTag == 4 {
            self.txtRestCity.text = arrCities?[row].city
            strSelectedCityId = "\(arrCities?[row].city_id ?? 0)"
        }
    }
}

extension RestProfileVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        intTag = textField.tag
        myPickerView.reloadAllComponents()
    }
}

//
//  RestTransactionHistoryVC.swift
//  BitesFunds
//
//  Created by Apple on 10/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class RestTransactionHistoryVC: UIViewController {

    // MARK: - Variables
    var arrTransactionHistory:[TransactionHistoryList] = []
    var arrSearchedTransactionHistory:[TransactionHistoryList]?
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewLogout: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        transactionHistoryAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        viewLogout.shadowToView()
        navigationController?.navigationBar.isHidden = true
        table.rowHeight = 75
    }
    //MARK: - Webservice
    func transactionHistoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        NetworkManager.shared.webserviceCallTransactionHistory(url: "\(URLs.restaurant_transaction_history)?page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.last_page = response.data?.last_page ?? 0
                self.arrTransactionHistory += (response.data?.data ?? [])
                self.arrSearchedTransactionHistory = self.arrTransactionHistory
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    // MARK: - IBAction
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
    @IBAction func btnLogout(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: false)
        Helper.shared.restaurantLogout()
    }

    @IBAction func btnSearch(_ sender: Any) {
        
        if txtSearch.text?.isEmpty ?? true
        {
            return
        }
        
        arrSearchedTransactionHistory = arrTransactionHistory.filter { ($0.user_name?.localizedCaseInsensitiveContains(txtSearch?.text ?? ""))! }
        
        self.table.reloadData()
    }
}
extension RestTransactionHistoryVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchedTransactionHistory?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionHistoryCell") as! TransactionHistoryCell
        
        let dict = arrSearchedTransactionHistory?[indexPath.row]
        cell.lblFirst.text = dict?.user_name
        cell.lblSecond.text = dict?.date
        cell.lblThird.text = dict?.bite
        cell.lblFifth.text = dict?.balance
        
        if dict?.transaction_type == "buy_bite" {
            cell.lblFourth.text = "BITE Bought (+)"
        }
        if dict?.transaction_type == "create_voucher" {
            cell.lblFourth.text = "Voucher Created (-)"
        }
        if dict?.transaction_type == "cancel_voucher" {
            cell.lblFourth.text = "Voucher Cancelled (+)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrTransactionHistory.count {
            if (indexPath.row == (self.arrSearchedTransactionHistory?.count ?? 0) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrSearchedTransactionHistory?.count ?? 0
                    transactionHistoryAPI()
                }
                
            }
        }
    }
}
extension RestTransactionHistoryVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength = (textField.text!.count) + (string.count) - range.length

        if newLength == 0 {
            self.arrSearchedTransactionHistory = self.arrTransactionHistory
            table.reloadData()
        }
        
        return true
    }
}

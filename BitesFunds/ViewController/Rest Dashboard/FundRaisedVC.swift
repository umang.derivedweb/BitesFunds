//
//  FundRaisedVC.swift
//  BitesFunds
//
//  Created by Apple on 10/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class FundRaisedVC: UIViewController {

    // MARK: - Variables
    var arrFundList:[FundsRaisedData] = []
    var arrSearchedFundList:[FundsRaisedData]?
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var txtSeach: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        fundRaisedListAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        viewLogout.shadowToView()
        navigationController?.navigationBar.isHidden = true
        table.rowHeight = 75
    }
    
    //MARK: - Webservice
    func fundRaisedListAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        NetworkManager.shared.webserviceCallFundRaisedList(url: "\(URLs.get_total_funds_raised_list)?page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.last_page = response.data!.last_page ?? 0
                self.arrFundList += (response.data?.data ?? [])
                self.arrSearchedFundList = self.arrFundList
                self.table.reloadData()
                
                self.lblHeader.text = "TOTAL RAISED: \(response.total_funds_raised ?? "") USD"
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    // MARK: - IBAction
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        
        if viewLogout.isHidden == false {
            viewLogout.isHidden = true
        } else {
            viewLogout.isHidden = false
        }
    }
    @IBAction func btnLogout(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: false)
        Helper.shared.restaurantLogout()
    }

    @IBAction func btnSearch(_ sender: Any) {
        
        if txtSeach.text?.isEmpty ?? true
        {
            return
        }
        
        arrSearchedFundList = arrFundList.filter { ($0.user_name?.localizedCaseInsensitiveContains(txtSeach?.text ?? ""))! }
        
        self.table.reloadData()
    }
}
extension FundRaisedVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchedFundList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FundRaisedCell") as! FundRaisedCell
        let dict = arrSearchedFundList?[indexPath.row]
        cell.lblFirst.text = dict?.user_name
        cell.lblSecond.text = dict?.date
        cell.lblThird.text = "\(dict?.bite ?? "") USD"
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrSearchedFundList?.count {
            if (indexPath.row == (self.arrSearchedFundList?.count ?? 0) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrSearchedFundList?.count ?? 0
                    fundRaisedListAPI()
                }
                
            }
        }
    }
}

extension FundRaisedVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newLength = (textField.text!.count) + (string.count) - range.length

        if newLength == 0 {
            self.arrSearchedFundList = self.arrFundList
            table.reloadData()
        }
        
        return true
    }
}

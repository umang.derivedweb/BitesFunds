//
//  Intro1VC.swift
//  BitesFunds
//
//  Created by Apple on 11/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class Intro1VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnGetStarted(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "Intro2VC") as! Intro2VC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func unwindToIntroVC(segue: UIStoryboardSegue) {
    }

}

//
//  Intro2VC.swift
//  BitesFunds
//
//  Created by Apple on 11/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class Intro2VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnHowWorks(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "Intro3VC") as! Intro3VC
        navigationController?.pushViewController(obj, animated: true)
    }

}

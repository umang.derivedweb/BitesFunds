//
//  RegisterVC.swift
//  BitesFunds
//
//  Created by Apple on 08/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import TezosKit

protocol LoginMessage {
    func sendLoginMessage(msg:String)
}

class RegisterVC: UIViewController {
    
    // MARK: - Variables
    public var wallet: KeychainWallet? = nil
    var strWalletToken:String = ""
    var delegate:LoginMessage?
    
    // MARK: - IBOutlet
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var txtFirstName: FormTextField!
    @IBOutlet weak var txtLastName: FormTextField!
    @IBOutlet weak var txtEmail: FormTextField!
    @IBOutlet weak var txtPassword: FormTextField!
    @IBOutlet weak var txtConfPassword: FormTextField!
    @IBOutlet weak var txtPhone: FormTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        generateWallet()
    }
    
    
   // MARK: - Function
    
    func setupUI() {
        
        viewBack.shadowToView()
    }
    
    func generateWallet() {
        self.wallet = KeychainWallet(passphrase: "\(Date().timeIntervalSince1970)")
        print(self.wallet?.getMnemonic())
        print(self.wallet?.getAddress())
        strWalletToken = "\(self.wallet?.getAddress() ?? "")"
        UserDefaults.standard.set("\(self.wallet?.getAddress() ?? "")", forKey: "wallet_token")
        UserDefaults.standard.synchronize()
    }
    // MARK: - Webservice
    
    func registerAPI() {
        Helper.shared.showHUD()
        let params = ["first_name":txtFirstName.text!,
                       "last_name":txtLastName.text!,
                       "email":txtEmail.text!,
                       "password":txtPassword.text!,
                       "phone_no":txtPhone.text!,
                       "crypto_wallet_token":strWalletToken] as [String : Any]
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallUserLoginSignUp(url: URLs.user_register, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtFirstName.text = ""
                self.txtLastName.text = ""
                self.txtEmail.text = ""
                self.txtPassword.text = ""
                
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
                //let seconds = 1.5
                //DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                    // Put your code which should be executed with a delay here
                self.navigationController?.popViewController(animated: true)
                self.delegate?.sendLoginMessage(msg: "Congratulation, your account has been created! Please login to your account below.")
                    
                //}
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShowPassword(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            if sender.image(for: .normal) != #imageLiteral(resourceName: "hide_eye") {
                sender.setImage(#imageLiteral(resourceName: "hide_eye"), for: .normal)
                sender.image(for: .normal)
                txtPassword.isSecureTextEntry = false
            } else {
                sender.setImage(#imageLiteral(resourceName: "eye"), for: .normal)
                txtPassword.isSecureTextEntry = true
            }
            
        } else {
           if sender.image(for: .normal) != #imageLiteral(resourceName: "hide_eye") {
               sender.setImage(#imageLiteral(resourceName: "hide_eye"), for: .normal)
               sender.image(for: .normal)
               txtConfPassword.isSecureTextEntry = false
           } else {
               sender.setImage(#imageLiteral(resourceName: "eye"), for: .normal)
               txtConfPassword.isSecureTextEntry = true
           }
        }
        
    }
    @IBAction func btnSubmit(_ sender: Any) {
        
        if txtFirstName.text?.isEmpty ?? true {
            Toast.show(message: Message.enterFName, controller: self)
            return
        }
        if txtLastName.text?.isEmpty ?? true {
            Toast.show(message: Message.enterLName, controller: self)
            return
        }
        
        if txtEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !(txtEmail.text)!.isValidEmail() {
                Toast.show(message: Message.enterValidEmail, controller: self)
                return
            }
        }
        if txtPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if txtPhone.text?.isEmpty ?? true {
            Toast.show(message: Message.enterNumber, controller: self)
            return
        } else {
            if !(txtPhone.text)!.isValidContact() {
                Toast.show(message: Message.enterValidNumber, controller: self)
                return
            }
        }
        registerAPI()
    }
    
}

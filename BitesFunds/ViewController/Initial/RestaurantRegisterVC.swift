//
//  RestaurantRegisterVC.swift
//  BitesFunds
//
//  Created by Apple on 12/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import TezosKit

class RestaurantRegisterVC: UIViewController {
    
    // MARK: - Variables
    var arrCities:[CitiesListData]?
    var arrStates:[StateData]?
    var myPickerView: UIPickerView!
    var toolBar:UIToolbar!
    var strSelectedCityId:String?
    var strSelectedRestType:String?
    var isHideSignIn:Bool?
    var strState_id:String?
    var intTag:Int?
    var arrRestType:[RestTypeData]?
    public var wallet: KeychainWallet? = nil
    var strWalletToken:String = ""
    
    // MARK: - IBOutlet
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var txtRestaurantName: FormTextField!
    @IBOutlet weak var txtEmail: FormTextField!
    @IBOutlet weak var txtPassword: FormTextField!
    @IBOutlet weak var txtConfPassword: FormTextField!
    @IBOutlet weak var txtSelectCity: FormTextField!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var txtState: FormTextField!
    @IBOutlet weak var txtRestType: FormTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        getStatesAPI()
        getRestTypeAPI()
        generateWallet()
    }
    
    
   // MARK: - Function
    
    func setupUI() {
        
        if isHideSignIn == true {
            btnSignIn.isHidden = true
        }
        viewBack.shadowToView()
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        txtSelectCity.inputView = self.myPickerView
        txtState.inputView = self.myPickerView
        txtRestType.inputView = self.myPickerView

        // ToolBar
        toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtSelectCity.inputAccessoryView = toolBar
        txtState.inputAccessoryView = toolBar
        txtRestType.inputAccessoryView = toolBar
    }
    func generateWallet() {
        self.wallet = KeychainWallet(passphrase: "\(Date().timeIntervalSince1970)")
        print(self.wallet?.getMnemonic())
        print(self.wallet?.getAddress())
        strWalletToken = "\(self.wallet?.getAddress() ?? "")"
        UserDefaults.standard.set("\(self.wallet?.getAddress() ?? "")", forKey: "wallet_token")
        UserDefaults.standard.synchronize()
    }
    
    @objc func doneClick() {
        self.view.endEditing(true)
        
        if intTag == 1 {
            getCitiesAPI()
        }
    }
    
    @objc func cancelClick() {
      self.view.endEditing(true)
    }
    
    @objc func btnOkay() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webservice
    
    func registerAPI() {
        Helper.shared.showHUD()
        let params = ["restaurant_name":txtRestaurantName.text!,
                      "city_id":strSelectedCityId!,
                       "email":txtEmail.text!,
                       "password":txtPassword.text!,
                       "restaurant_type":strSelectedRestType!,
                       "crypto_wallet_token":strWalletToken] as [String : Any]
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallRestLoginSignUp(url: URLs.restaurant_register, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtRestaurantName.text = ""
                self.txtSelectCity.text = ""
                self.txtEmail.text = ""
                self.txtPassword.text = ""
                self.txtState.text = ""
                self.txtRestType.text = ""
                self.view.endEditing(true)
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
                //let seconds = 1.5
//                DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
//                    // Put your code which should be executed with a delay here
//                    self.navigationController?.popViewController(animated: true)
//                }
                
                let customView = Bundle.main.loadNibNamed("SignUpView", owner: self, options: nil)?.first as! SignUpView
                customView.btnOkay.addTarget(self, action: #selector(self.btnOkay), for: .touchUpInside)
                customView.tag = 1001
                customView.frame = self.view.frame
                self.view.addSubview(customView)
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    func getCitiesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCities(url: URLs.get_cities+"?state_id=\(strState_id ?? "")", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCities = response.data?.data
                self.myPickerView.reloadAllComponents()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    func getStatesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetStates(url: URLs.get_states, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrStates = response.data?.data
                self.myPickerView.reloadAllComponents()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    func getRestTypeAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.restaurant_token)"]
        NetworkManager.shared.webserviceCallRestType(url: URLs.get_restaurant_type, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrRestType = response.data
                self.myPickerView.reloadAllComponents()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShowPassword(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            if sender.image(for: .normal) != #imageLiteral(resourceName: "hide_eye") {
                sender.setImage(#imageLiteral(resourceName: "hide_eye"), for: .normal)
                sender.image(for: .normal)
                txtPassword.isSecureTextEntry = false
            } else {
                sender.setImage(#imageLiteral(resourceName: "eye"), for: .normal)
                txtPassword.isSecureTextEntry = true
            }
            
        } else {
           if sender.image(for: .normal) != #imageLiteral(resourceName: "hide_eye") {
               sender.setImage(#imageLiteral(resourceName: "hide_eye"), for: .normal)
               sender.image(for: .normal)
               txtConfPassword.isSecureTextEntry = false
           } else {
               sender.setImage(#imageLiteral(resourceName: "eye"), for: .normal)
               txtConfPassword.isSecureTextEntry = true
           }
        }
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        if txtRestaurantName.text?.isEmpty ?? true {
            Toast.show(message: Message.enterRestaurant, controller: self)
            return
        }
        if txtSelectCity.text?.isEmpty ?? true {
            Toast.show(message: Message.selectCity, controller: self)
            return
        }
        
        if txtEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !(txtEmail.text)!.isValidEmail() {
                Toast.show(message: Message.enterValidEmail, controller: self)
                return
            }
        }
        if txtPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if txtRestType.text?.isEmpty ?? true {
            Toast.show(message: Message.selectRestType, controller: self)
            return
        }
        registerAPI()
    }
    
}

extension RestaurantRegisterVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if intTag == 1 {
            return arrStates?.count ??  0
        } else if intTag == 2 {
            return arrCities?.count ??  0
        } else {
            return arrRestType?.count ??  0
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if intTag == 1 {
            return arrStates?[row].state_name
        } else if intTag == 2 {
            return arrCities?[row].city
        } else {
            return arrRestType?[row].restaurant_type
        }
        
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if intTag == 1 {
            self.txtState.text = arrStates?[row].state_name
            strState_id = "\(arrStates?[row].state_id ?? "0")"
            
        } else if intTag == 2 {
            self.txtSelectCity.text = arrCities?[row].city
            strSelectedCityId = "\(arrCities?[row].city_id ?? 0)"
        }  else if intTag == 3 {
            self.txtRestType.text = arrRestType?[row].restaurant_type
            strSelectedRestType = "\(arrRestType?[row].restaurant_type_id ?? 0)"
        }
        
    }
    
}

extension RestaurantRegisterVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 2 {
            
            if strState_id == nil {
                txtSelectCity.inputView = UIView()
                txtSelectCity.inputAccessoryView = UIView()
                Toast.show(message: "Please select State", controller: self)
                return
            } else {
                txtSelectCity.inputView = myPickerView
                txtSelectCity.inputAccessoryView = toolBar
            }
        }
        intTag = textField.tag
        myPickerView.reloadAllComponents()
    }
}

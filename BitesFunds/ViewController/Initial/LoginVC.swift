//
//  LoginVC.swift
//  BitesFunds
//
//  Created by Apple on 08/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import MMDrawerController

protocol isLogin {
    
    func sendData()
}

class LoginVC: UIViewController, LoginMessage {
    
    

    // MARK: - Variable
    var strFrom = ""
    var delegae:isLogin?
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var txtEmail: FormTextField!
    @IBOutlet weak var txtPassword: FormTextField!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblLogin: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
//        let obj = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "SupportRestTabbar") as! UITabBarController
//        navigationController?.pushViewController(obj, animated: false)
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        viewBack.shadowToView()
        if Helper.shared.strLoginType == "User" {
            lblLogin.text = "USER LOGIN"
            if UserDefaults.standard.value(forKey: "isUserLogin") != nil && UserDefaults.standard.value(forKey: "isUserLogin") as! Bool == true {
                Helper.shared.userId = UserDefaults.standard.value(forKey: "userId") as! String
                Helper.shared.user_token = UserDefaults.standard.value(forKey: "api_token") as! String
                Helper.shared.user_name = UserDefaults.standard.value(forKey: "userName") as! String
                Helper.shared.user_profile_pic = UserDefaults.standard.value(forKey: "profile_pic") as! String
                
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                
                let centerViewController = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "UserDashboardVC") as! UserDashboardVC
                let leftViewController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "GeneralMenuVC") as! GeneralMenuVC
                
                let leftSideNav = UINavigationController(rootViewController: leftViewController)
                leftSideNav.navigationBar.isHidden = true
                let centerNav = UINavigationController(rootViewController: centerViewController)
                centerNav.navigationBar.isHidden = true
                appDelegate.centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
                
                appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
                appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapCenterView
                //window?.rootViewController = centerContainer
                appDelegate.centerContainer?.maximumLeftDrawerWidth = 350
                
                self.navigationController?.pushViewController(appDelegate.centerContainer!, animated: false)
            }
            
        } else {
            lblLogin.text = "RESTAURANT LOGIN"
            if UserDefaults.standard.value(forKey: "isRestLogin") != nil && UserDefaults.standard.value(forKey: "isRestLogin") as! Bool == true {
                Helper.shared.restaurantId = UserDefaults.standard.value(forKey: "restaurant_Id") as! String
                Helper.shared.restaurant_token = UserDefaults.standard.value(forKey: "restaurant_token") as! String
                Helper.shared.restaurant_name = UserDefaults.standard.value(forKey: "restaurantName") as! String
                Helper.shared.restaurant_profile_pic = UserDefaults.standard.value(forKey: "restaurant_image") as! String
                Helper.shared.restaurant_description = UserDefaults.standard.value(forKey: "restaurant_description") as! String
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                let centerViewController = UIStoryboard(name: "RestaurantDashboard", bundle: nil).instantiateViewController(withIdentifier: "RestDashboardVC") as! RestDashboardVC
                let leftViewController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "GeneralMenuVC") as! GeneralMenuVC
                
                let leftSideNav = UINavigationController(rootViewController: leftViewController)
                leftSideNav.navigationBar.isHidden = true
                let centerNav = UINavigationController(rootViewController: centerViewController)
                centerNav.navigationBar.isHidden = true
                appDelegate.centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
                
                appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
                appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapCenterView
                //window?.rootViewController = centerContainer
                appDelegate.centerContainer?.maximumLeftDrawerWidth = 350
                
                self.navigationController?.pushViewController(appDelegate.centerContainer!, animated: false)
            }
        }
        
    }
    
    func sendLoginMessage(msg: String) {
        // create the alert
        let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Webservice
    
    func userLoginAPI() {
        Helper.shared.showHUD()
        let params = ["email":txtEmail.text!,
                       "password":txtPassword.text!] as [String : Any]
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallUserLoginSignUp(url: URLs.user_login, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtEmail.text = ""
                self.txtPassword.text = ""
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                
                Helper.shared.userId = "\(response.data?.user_id ?? 0)"
                Helper.shared.user_token = response.data!.api_token ?? ""
                Helper.shared.user_profile_pic = response.data?.profile_pic ?? ""
                Helper.shared.user_name = "\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")"
                
                UserDefaults.standard.set(true, forKey: "isUserLogin")
                UserDefaults.standard.set("\(response.data?.user_id ?? 0)", forKey: "userId")
                UserDefaults.standard.set(response.data?.api_token ?? "", forKey: "api_token")
                UserDefaults.standard.set(response.data?.profile_pic ?? "", forKey: "profile_pic")
                UserDefaults.standard.set("\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")", forKey: "userName")
                UserDefaults.standard.synchronize()
                
                
                if self.strFrom == "General" {
                    self.delegae?.sendData()
                    self.navigationController?.popViewController(animated: false)
                } else {
                    
                    
                    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    
                    let centerViewController = UIStoryboard(name: "UserDashboard", bundle: nil).instantiateViewController(withIdentifier: "UserDashboardVC") as! UserDashboardVC
                    let leftViewController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "GeneralMenuVC") as! GeneralMenuVC
                    
                    let leftSideNav = UINavigationController(rootViewController: leftViewController)
                    leftSideNav.navigationBar.isHidden = true
                    let centerNav = UINavigationController(rootViewController: centerViewController)
                    centerNav.navigationBar.isHidden = true
                    appDelegate.centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
                    
                    appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
                    appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapCenterView
                    //window?.rootViewController = centerContainer
                    appDelegate.centerContainer?.maximumLeftDrawerWidth = 350
                    
                    self.navigationController?.pushViewController(appDelegate.centerContainer!, animated: false)
                }
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    
    func restLoginAPI() {
        Helper.shared.showHUD()
        let params = ["email":txtEmail.text!,
                       "password":txtPassword.text!] as [String : Any]
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallRestLoginSignUp(url: URLs.restaurant_login, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtEmail.text = ""
                self.txtPassword.text = ""
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                
                Helper.shared.restaurantId = "\(response.data?.restaurant_id ?? 0)"
                Helper.shared.restaurant_token = response.data!.api_token ?? ""
                Helper.shared.restaurant_profile_pic = response.data?.restaurant_image ?? ""
                Helper.shared.restaurant_name = response.data?.restaurant_name ?? ""
                Helper.shared.restaurant_description = response.data?.description ?? ""
                
                UserDefaults.standard.set(true, forKey: "isRestLogin")
                UserDefaults.standard.set("\(response.data?.restaurant_id ?? 0)", forKey: "restaurant_Id")
                UserDefaults.standard.set(response.data?.api_token ?? "", forKey: "restaurant_token")
                UserDefaults.standard.set(response.data?.restaurant_image ?? "", forKey: "restaurant_image")
                UserDefaults.standard.set(response.data?.restaurant_name ?? "", forKey: "restaurantName")
                UserDefaults.standard.set(response.data?.description ?? "", forKey: "restaurant_description")
                UserDefaults.standard.synchronize()
                
                if self.strFrom == "General" {
                    self.delegae?.sendData()
                    self.navigationController?.popViewController(animated: false)
                } else {
                    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    
                     let centerViewController = UIStoryboard(name: "RestaurantDashboard", bundle: nil).instantiateViewController(withIdentifier: "RestDashboardVC") as! RestDashboardVC
                    let leftViewController = UIStoryboard(name: "General", bundle: nil).instantiateViewController(withIdentifier: "GeneralMenuVC") as! GeneralMenuVC
                    
                    let leftSideNav = UINavigationController(rootViewController: leftViewController)
                    leftSideNav.navigationBar.isHidden = true
                    let centerNav = UINavigationController(rootViewController: centerViewController)
                    centerNav.navigationBar.isHidden = true
                    appDelegate.centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
                    
                    appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
                    appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapCenterView
                    //window?.rootViewController = centerContainer
                    appDelegate.centerContainer?.maximumLeftDrawerWidth = 350
                    
                    self.navigationController?.pushViewController(appDelegate.centerContainer!, animated: false)
                    
                }
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnSubmit(_ sender: Any) {
        
        if txtEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !(txtEmail.text)!.isValidEmail() {
                Toast.show(message: Message.enterValidEmail, controller: self)
                return
            }
        }
        if txtPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        
        
        if Helper.shared.strLoginType == "User" {
            userLoginAPI()
        } else {
            restLoginAPI()
        }
        
    }
    @IBAction func btnSignUp(_ sender: Any) {
        if Helper.shared.strLoginType == "User" {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            obj.delegate = self
            navigationController?.pushViewController(obj, animated: true)
        } else {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantRegisterVC") as! RestaurantRegisterVC
            navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        Helper.shared.strLoginType = "General"
    }
    @IBAction func btnForgotPassword(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func unwindToLoginVC(segue: UIStoryboardSegue) {
    }
    
}

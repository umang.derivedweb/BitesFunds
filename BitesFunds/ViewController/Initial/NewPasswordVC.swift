//
//  NewPasswordVC.swift
//  WeParty
//
//  Created by Apple on 16/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewPasswordVC: UIViewController {

    // MARK: - Variables
    var strEmail:String?
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var txtNewPassword: FormTextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        viewBack.shadowToView()
    }
    
    
    // MARK: - Webservice
    
    func newPasswordAPI(url:String) {
        Helper.shared.showHUD()
        let params = ["email":strEmail!,
                      "new_password":txtNewPassword.text!,
                      "confirm_password":txtConfirmPassword.text!] as [String : Any]
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallCommon(url: url, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtNewPassword.text = ""
                self.txtConfirmPassword.text = ""
                self.view.endEditing(true)
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                let seconds = 1.5
                DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                     //Put your code which should be executed with a delay here
                    self.performSegue(withIdentifier: "segueUnwind", sender: self)
                }
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnChangePassword(_ sender: Any) {
        
        if txtNewPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if txtConfirmPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if txtConfirmPassword.text != txtNewPassword.text {
            Toast.show(message: Message.passwordUnmatched, controller: self)
            return
        }
        
        if Helper.shared.strLoginType == "User" {
            newPasswordAPI(url:URLs.user_new_password)
        } else {
            newPasswordAPI(url:URLs.restaurant_new_password)
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

//
//  ForgotPasswordVC.swift
//  BitesFunds
//
//  Created by Apple on 08/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class ForgotPasswordVC: UIViewController {
    
    // MARK: - Variables
    
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var txtEmail: FormTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    
    // MARK: - Function
    
    func setupUI() {
        
        viewBack.shadowToView()
    }
    @objc func btnVerifyOTP() {
        
        if Helper.shared.strLoginType == "User" {
            checkOTP_API(url:URLs.user_check_otp)
        } else {
            checkOTP_API(url:URLs.restaurant_check_otp)
        }
    }
    
    // MARK: - Webservice
    
    func forgotPasswordAPI(url:String) {
        Helper.shared.showHUD()
        let params = ["email":txtEmail.text!] as [String : Any]
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallCommon(url: url, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                
                let customView = Bundle.main.loadNibNamed("EnterOtpView", owner: self, options: nil)?.first as! EnterOtpView
                customView.btnVerifyOTP.addTarget(self, action: #selector(self.btnVerifyOTP), for: .touchUpInside)
                customView.tag = 1001
                customView.frame = self.view.frame
                self.view.addSubview(customView)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            SVProgressHUD.dismiss()
        }
    }
    
    func checkOTP_API(url:String) {
        Helper.shared.showHUD()
        
        if (self.view.viewWithTag(1001) as! EnterOtpView).txtOTP.text != "" {
            
            var params:[String:String]!
            
            if Helper.shared.strLoginType == "User" {
                params = ["email":txtEmail.text!,
                "OTP":(self.view.viewWithTag(1001) as! EnterOtpView).txtOTP.text ?? ""]
            } else {
                params = ["email":txtEmail.text!,
                "otp":(self.view.viewWithTag(1001) as! EnterOtpView).txtOTP.text ?? ""] 
            }
            
            
            let headers = ["Accept":"application/json"]
            
            NetworkManager.shared.webserviceCallCommon(url: url, parameters: params, headers:headers) { (response) in
                
                if response.ResponseCode == 200 {
                    self.view.viewWithTag(1001)?.removeFromSuperview()
                    
                    Toast.show(message: response.ResponseMsg ?? "", controller: self)
                    
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "NewPasswordVC") as! NewPasswordVC
                    obj.strEmail = self.txtEmail.text
                    self.navigationController?.pushViewController(obj, animated: true)
                    self.txtEmail.text = ""
                } else {
                    Toast.show(message: response.ResponseMsg ?? "", controller: self)
                }
                SVProgressHUD.dismiss()
            }
        }
        
    }
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        if txtEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !(txtEmail.text)!.isValidEmail() {
                Toast.show(message: Message.enterValidEmail, controller: self)
                return
            }
        }
        if Helper.shared.strLoginType == "User" {
            forgotPasswordAPI(url:URLs.user_forgot_password)
        } else {
            forgotPasswordAPI(url:URLs.restaurant_forgot_password)
        }
        
    }
}

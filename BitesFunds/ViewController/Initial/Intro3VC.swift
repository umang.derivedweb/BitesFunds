//
//  Intro3VC.swift
//  BitesFunds
//
//  Created by Apple on 11/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class Intro3VC: UIViewController {

    @IBOutlet weak var btnRest: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnRest.layer.borderColor = #colorLiteral(red: 0.5044823289, green: 0.1835186183, blue: 0.5145391226, alpha: 1)
        btnRest.layer.borderWidth = 1
    }
    
    @IBAction func btnUser(_ sender: Any) {
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        Helper.shared.strLoginType = "User"
        navigationController?.pushViewController(obj, animated: false)
    }
    
    @IBAction func btnRestaurant(_ sender: Any) {
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        Helper.shared.strLoginType = "Rest"
        navigationController?.pushViewController(obj, animated: false)
    }
    
    
}

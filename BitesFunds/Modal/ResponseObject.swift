//
//  ResponseObject.swift
//  BitesFunds
//
//  Created by Apple on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import ObjectMapper

class CommonResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
    }
    
}
class GetRestaurantsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:RestaurantsData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class GetRestDetailsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:GetRestDetailsData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class PaidBiteBoughtResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:PaidBiteBoughtData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class TotalFundRaisedResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:TotalFundRaisedData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class GetSupportRestResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:[SupportRestaurantData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class RestaurantDetailsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:RestaurantListData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class UserRegisterResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:UserRegisterData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class RestRegisterResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:RestRegisterData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class GetCitiesResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:GetCitiesData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class UserDashboardResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:UserDashboardData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class VoucherDetailsResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:VoucherDetailsData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class UserGetProfileResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:UserGetProfileData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class RestGetProfileResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:RestGetProfileData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class RestTypeResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:[RestTypeData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ReedemedVoucherListResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:ReedemedVoucherListData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class TransactionHistoryResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:TransactionHistoryData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class BiteBoughtListResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:BiteBoughtData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class FundsRaisedListResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:FundsRaisedListData?
    var total_funds_raised:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
        total_funds_raised <- map["total_funds_raised"]
    }
    
}
class GetStatesResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:GetStateData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class BuyBiteResponse: Mappable {
    
    var ResponseCode:Int?
    var ResponseMsg:String?
    var data:BuyBiteData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseCode <- map["ResponseCode"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

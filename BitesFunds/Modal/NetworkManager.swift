//
//  NetworkManager.swift
//  BitesFunds
//
//  Created by Apple on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit
import AlamofireObjectMapper
import Alamofire

class NetworkManager: NSObject {

    static let shared = NetworkManager()
    
    
    func webserviceCallGetRest(url:String, headers:HTTPHeaders, completion:@escaping (GetRestaurantsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<GetRestaurantsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    
    func webserviceCallGetRestDetails(url:String, headers:HTTPHeaders, completion:@escaping (GetRestDetailsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<GetRestDetailsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetSupportRest(url:String, headers:HTTPHeaders, completion:@escaping (GetSupportRestResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<GetSupportRestResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallUserLoginSignUp(url:String, parameters:[String:Any], headers:HTTPHeaders, completion:@escaping (UserRegisterResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<UserRegisterResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallRestLoginSignUp(url:String, parameters:[String:Any], headers:HTTPHeaders, completion:@escaping (RestRegisterResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<RestRegisterResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallBuyBite(url:String, parameters:[String:Any], headers:HTTPHeaders, completion:@escaping (BuyBiteResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<BuyBiteResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetCities(url:String, headers:HTTPHeaders, completion:@escaping (GetCitiesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<GetCitiesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallUserDashboard(url:String, headers:HTTPHeaders, completion:@escaping (UserDashboardResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<UserDashboardResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallUserVoucherDetails(url:String, headers:HTTPHeaders, completion:@escaping (VoucherDetailsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<VoucherDetailsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallUserGetProfile(url:String, headers:HTTPHeaders, completion:@escaping (UserGetProfileResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<UserGetProfileResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallRestGetProfile(url:String, headers:HTTPHeaders, completion:@escaping (RestGetProfileResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<RestGetProfileResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallPaidBiteBought(url:String, headers:HTTPHeaders, completion:@escaping (PaidBiteBoughtResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<PaidBiteBoughtResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallTotalFundRaised(url:String, headers:HTTPHeaders, completion:@escaping (TotalFundRaisedResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<TotalFundRaisedResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallRestType(url:String, headers:HTTPHeaders, completion:@escaping (RestTypeResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<RestTypeResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallTransactionHistory(url:String, headers:HTTPHeaders, completion:@escaping (TransactionHistoryResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<TransactionHistoryResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetStates(url:String, headers:HTTPHeaders, completion:@escaping (GetStatesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<GetStatesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallBiteBoughtList(url:String, headers:HTTPHeaders, completion:@escaping (BiteBoughtListResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<BiteBoughtListResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallFundRaisedList(url:String, headers:HTTPHeaders, completion:@escaping (FundsRaisedListResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<FundsRaisedListResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallReedemedVoucherList(url:String, headers:HTTPHeaders, completion:@escaping (ReedemedVoucherListResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, headers: headers).responseObject { (response: DataResponse<ReedemedVoucherListResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallCommon(url:String, parameters:[String:Any], headers:HTTPHeaders, completion:@escaping (CommonResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<CommonResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    
    func webserviceCallEditProfile(url:String, parameters:[String:String], imgData:Data, headers:HTTPHeaders, completion:@escaping (CommonResponse)->()){
        

        if Reachability.isConnectedToNetwork() {

            Alamofire.upload(multipartFormData: { multipartFormData in
                    //loop this "multipartFormData" and make the key as array data
                    multipartFormData.append(imgData, withName: "profile_image",fileName: "\(Int.random(in: 0..<10000)).jpg", mimeType: "image/jpg")
                    
                    for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                        } //Optional for extra parameters
                },
            to:url,headers: headers)
            { (result) in
                switch result {
                case .success(let upload, _, _):

                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })

                    upload.responseObject { (response: DataResponse<CommonResponse>) in
                        print(response.result.value)
                        if let _ = response.result.value {
                            
                            completion(response.result.value!)
                        } else {
                            
                        }
                    }

                case .failure(let encodingError):
                    print(encodingError)
                }
            }
        } else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallRestEditProfile(url:String, parameters:[String:String], imgData:Data, headers:HTTPHeaders, completion:@escaping (CommonResponse)->()){
        

        if Reachability.isConnectedToNetwork() {

            Alamofire.upload(multipartFormData: { multipartFormData in
                    //loop this "multipartFormData" and make the key as array data
                    multipartFormData.append(imgData, withName: "restaurant_image",fileName: "\(Int.random(in: 0..<10000)).jpg", mimeType: "image/jpg")
                    
                    for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                        } //Optional for extra parameters
                },
            to:url,headers: headers)
            { (result) in
                switch result {
                case .success(let upload, _, _):

                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })

                    upload.responseObject { (response: DataResponse<CommonResponse>) in
                        print(response.result.value)
                        if let _ = response.result.value {
                            
                            completion(response.result.value!)
                        } else {
                            
                        }
                    }

                case .failure(let encodingError):
                    print(encodingError)
                }
            }
        } else{
            print("Internet Connection not Available!")
        }
        
    }
}

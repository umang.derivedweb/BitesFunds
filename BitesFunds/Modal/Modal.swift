//
//  Modal.swift
//  BitesFunds
//
//  Created by Apple on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import ObjectMapper


class RestaurantsData: Mappable {
    
    var current_page:Int?
    var first_page_url:String?
    var from:Int?
    var last_page:Int?
    var last_page_url:String?
    var next_page_url:String?
    var path:String?
    var per_page:Int?
    var prev_page_url:String?
    var to:Int?
    var total:Int?
    var data:[RestaurantListData]?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        current_page <- map["current_page"]
        first_page_url <- map["first_page_url"]
        from <- map["from"]
        last_page <- map["last_page"]
        last_page_url <- map["last_page_url"]
        next_page_url <- map["next_page_url"]
        path <- map["path"]
        per_page <- map["per_page"]
        prev_page_url <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
        data <- map["data"]
        
    }
    
}
class RestaurantListData: Mappable {
    
    var restaurant_id:Int?
    var email:String?
    var restaurant_name:String?
    var restaurant_image:String?
    var restaurant_type:String?
    var description:String?
    var latitude:String?
    var longitude:String?
    var city:String?
    var created_at:String?
    var otp:String?
    var api_token:String?
    var offer:String?
    var percentage_you_want:String?
    var is_approved:String?
    var city_id:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        restaurant_id <- map["restaurant_id"]
        email <- map["email"]
        restaurant_name <- map["restaurant_name"]
        restaurant_image <- map["restaurant_image"]
        restaurant_type <- map["restaurant_type"]
        description <- map["description"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        city <- map["city"]
        created_at <- map["created_at"]
        otp <- map["otp"]
        api_token <- map["api_token"]
        offer <- map["offer"]
        percentage_you_want <- map["percentage_you_want"]
        is_approved <- map["is_approved"]
        city_id <- map["city_id"]
    }
    
}

class GetRestDetailsData: Mappable {
    
    var restaurant_id:Int?
    var email:String?
    var restaurant_name:String?
    var restaurant_image:String?
    var restaurant_type:String?
    var description:String?
    var latitude:String?
    var longitude:String?
    var city:String?
    var created_at:String?
    var otp:String?
    var api_token:String?
    var offer:String?
    var percentage_you_want:Int?
    var is_approved:Int?
    var city_id:Int?
    var bite_in_wallet:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        restaurant_id <- map["restaurant_id"]
        email <- map["email"]
        restaurant_name <- map["restaurant_name"]
        restaurant_image <- map["restaurant_image"]
        restaurant_type <- map["restaurant_type"]
        description <- map["description"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        city <- map["city"]
        created_at <- map["created_at"]
        otp <- map["otp"]
        api_token <- map["api_token"]
        offer <- map["offer"]
        percentage_you_want <- map["percentage_you_want"]
        is_approved <- map["is_approved"]
        city_id <- map["city_id"]
        bite_in_wallet <- map["bite_in_wallet"]
    }
    
}

class PaidBiteBoughtData: Mappable {
    
    var usd_paid:String?
    var bite_bought:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        
        usd_paid <- map["usd_paid"]
        bite_bought <- map["bite_bought"]
    }
    
}

class TotalFundRaisedData: Mappable {
    
    var total_funds_raised:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        
        total_funds_raised <- map["total_funds_raised"]
    }
    
}
class SupportRestaurantData: Mappable {
    
    
    var restaurant_name:String?
    var restaurant_image:String?
    var city:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        
        restaurant_name <- map["restaurant_name"]
        restaurant_image <- map["restaurant_image"]
        city <- map["city"]
        
    }
    
}
class UserRegisterData: Mappable {
    
    var user_id:Int?
    var profile_pic:String?
    var first_name:String?
    var last_name:String?
    var email:String?
    var phone_no:String?
    var otp:String?
    var api_token:String?
    var created_at:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        profile_pic <- map["profile_pic"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        email <- map["email"]
        phone_no <- map["phone_no"]
        otp <- map["otp"]
        api_token <- map["api_token"]
        created_at <- map["created_at"]
    }
    
}
class RestRegisterData: Mappable {
    
    var restaurant_id:Int?
    var restaurant_image:String?
    var restaurant_name:String?
    var email:String?
    var restaurant_type:Int?
    var description:String?
    var api_token:String?
    var otp:String?
    var city_id:Int?
    var offer:String?
    var percentage_you_want:String?
    var is_approved:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        restaurant_id <- map["restaurant_id"]
        restaurant_image <- map["restaurant_image"]
        restaurant_name <- map["restaurant_name"]
        email <- map["email"]
        restaurant_type <- map["restaurant_type"]
        description <- map["description"]
        api_token <- map["api_token"]
        otp <- map["otp"]
        city_id <- map["city_id"]
        offer <- map["offer"]
        percentage_you_want <- map["percentage_you_want"]
        is_approved <- map["is_approved"]
    }
    
}

class GetCitiesData: Mappable {
    
    var data:[CitiesListData]?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}
class CitiesListData: Mappable {
    
    var city_id:Int?
    var city:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        city_id <- map["city_id"]
        city <- map["city"]
        
    }
    
}

class UserDashboardData: Mappable {
    
    var bite_in_wallet:String?
    var active_voucher:String?
    var total_active_voucher_bite:String?
    var vouchers:VoucherData?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        bite_in_wallet <- map["bite_in_wallet"]
        active_voucher <- map["active_voucher"]
        total_active_voucher_bite <- map["total_active_voucher_bite"]
        vouchers <- map["vouchers"]
        
    }
    
}

class VoucherData: Mappable {
    
    var current_page:Int?
    var first_page_url:String?
    var from:Int?
    var last_page:Int?
    var last_page_url:String?
    var next_page_url:String?
    var path:String?
    var per_page:Int?
    var prev_page_url:String?
    var to:Int?
    var total:Int?
    var data:[VoucherListData]?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        first_page_url <- map["first_page_url"]
        from <- map["from"]
        last_page <- map["last_page"]
        last_page_url <- map["last_page_url"]
        next_page_url <- map["next_page_url"]
        path <- map["path"]
        per_page <- map["per_page"]
        prev_page_url <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
    }
    
}
class VoucherListData: Mappable {
    
    var voucher_id:Int?
    var restaurant_name:String?
    var voucher_code:String?
    var bite:String?
    var date:String?
    var status:String?
    var name:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        voucher_id <- map["voucher_id"]
        restaurant_name <- map["restaurant_name"]
        voucher_code <- map["voucher_code"]
        bite <- map["bite"]
        date <- map["date"]
        status <- map["status"]
        name <- map["name"]
    }
    
}

class VoucherDetailsData: Mappable {
    
    var voucher_id:Int?
    var restaurant_name:String?
    var voucher_code:String?
    var bite:String?
    var date_added:String?
    var restaurant_image:String?
    var offer:String?
    var percentage_you_want:String?
    var status:String?
    var qr_url:String?
    var first_name:String?
    var last_name:String?
    var profile_pic:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        voucher_id <- map["voucher_id"]
        restaurant_name <- map["restaurant_name"]
        voucher_code <- map["voucher_code"]
        bite <- map["bite"]
        date_added <- map["date_added"]
        restaurant_image <- map["restaurant_image"]
        offer <- map["offer"]
        percentage_you_want <- map["percentage_you_want"]
        qr_url <- map["qr_url"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        profile_pic <- map["profile_pic"]
        status <- map["status"]
    }
    
}

class UserGetProfileData: Mappable {
    
    var user_id:Int?
    var profile_pic:String?
    var first_name:String?
    var last_name:String?
    var email:String?
    var otp:String?
    var phone_no:String?
    var api_token:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        profile_pic <- map["profile_pic"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        email <- map["email"]
        otp <- map["otp"]
        phone_no <- map["phone_no"]
        api_token <- map["api_token"]
    }
    
}

class RestGetProfileData: Mappable {
    
    var restaurant_id:Int?
    var restaurant_image:String?
    var restaurant_name:String?
    var restaurant_type:String?
    var email:String?
    var otp:String?
    var description:String?
    var api_token:String?
    
    var city_id:String?
    var state_id:String?
    var state_name:String?
    var city:String?
    var offer:String?
    var percentage_you_want:String?
    var is_approved:Int?
    var restaurant_type_id:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        restaurant_id <- map["restaurant_id"]
        restaurant_image <- map["restaurant_image"]
        restaurant_name <- map["restaurant_name"]
        restaurant_type <- map["restaurant_type"]
        email <- map["email"]
        otp <- map["otp"]
        description <- map["description"]
        api_token <- map["api_token"]
        restaurant_type_id <- map["restaurant_type_id"]
        state_name <- map["state_name"]
        state_id <- map["state_id"]
        city <- map["city"]
        city_id <- map["city_id"]
        offer <- map["offer"]
        percentage_you_want <- map["percentage_you_want"]
        is_approved <- map["is_approved"]
    }
    
}
class RestTypeData: Mappable {
    
    var restaurant_type_id:Int?
    var restaurant_type:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        restaurant_type_id <- map["restaurant_type_id"]
        restaurant_type <- map["restaurant_type"]
        
    }
    
}

class ReedemedVoucherListData: Mappable {
    
    var total_redeemed_bite:String?
    var redeemed_vouchers:RedeemedVouchersData?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        total_redeemed_bite <- map["total_redeemed_bite"]
        redeemed_vouchers <- map["redeemed_vouchers"]
        
    }
    
}
class RedeemedVouchersData: Mappable {
    
    var current_page:Int?
    var first_page_url:String?
    var from:Int?
    var last_page:Int?
    var last_page_url:String?
    var next_page_url:String?
    var path:String?
    var per_page:Int?
    var prev_page_url:String?
    var to:Int?
    var total:Int?
    var data:[RedeemedVouchersList]?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        first_page_url <- map["first_page_url"]
        from <- map["from"]
        last_page <- map["last_page"]
        last_page_url <- map["last_page_url"]
        next_page_url <- map["next_page_url"]
        path <- map["path"]
        per_page <- map["per_page"]
        prev_page_url <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
        
    }
    
}
class RedeemedVouchersList: Mappable {
    
    var user_name:String?
    var voucher_code:String?
    var bite:String?
    var date_of_redeemed:String?
    var status:String?
    var voucher_id:Int?
    
    //Restaurant
    var date_added:String?
    var restaurant_name:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        user_name <- map["user_name"]
        voucher_code <- map["voucher_code"]
        bite <- map["bite"]
        date_of_redeemed <- map["date_of_redeemed"]
        status <- map["status"]
        voucher_id <- map["voucher_id"]
        
        date_added <- map["date_added"]
        restaurant_name <- map["restaurant_name"]
    }
    
}

class TransactionHistoryData: Mappable {
    
    var current_page:Int?
    var first_page_url:String?
    var from:Int?
    var last_page:Int?
    var last_page_url:String?
    var next_page_url:String?
    var path:String?
    var per_page:Int?
    var prev_page_url:String?
    var to:Int?
    var total:Int?
    var data:[TransactionHistoryList]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        first_page_url <- map["first_page_url"]
        from <- map["from"]
        last_page <- map["last_page"]
        last_page_url <- map["last_page_url"]
        next_page_url <- map["next_page_url"]
        path <- map["path"]
        per_page <- map["per_page"]
        prev_page_url <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
    }
    
}
class TransactionHistoryList: Mappable {
    
    var user_name:String?
    var balance:String?
    var bite:String?
    var date:String?
    var transaction_type:String?
    
    //Restaurant
    var user_transaction_id:Int?
    var restaurant_name:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        user_name <- map["user_name"]
        balance <- map["balance"]
        bite <- map["bite"]
        date <- map["date"]
        transaction_type <- map["transaction_type"]
        
        user_transaction_id <- map["user_transaction_id"]
        restaurant_name <- map["restaurant_name"]
        
    }
    
}

class BiteBoughtData: Mappable {
    
    var current_page:Int?
    var first_page_url:String?
    var from:Int?
    var last_page:Int?
    var last_page_url:String?
    var next_page_url:String?
    var path:String?
    var per_page:Int?
    var prev_page_url:String?
    var to:Int?
    var total:Int?
    var data:[BiteBoughtDataListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        first_page_url <- map["first_page_url"]
        from <- map["from"]
        last_page <- map["last_page"]
        last_page_url <- map["last_page_url"]
        next_page_url <- map["next_page_url"]
        path <- map["path"]
        per_page <- map["per_page"]
        prev_page_url <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
    }
    
}

class BiteBoughtDataListData: Mappable {
    
    var restaurant_name:String?
    var usd_spent:String?
    var bite:String?
    var date:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        restaurant_name <- map["restaurant_name"]
        usd_spent <- map["usd_spent"]
        bite <- map["bite"]
        date <- map["date"]
        
    }
    
}

class FundsRaisedListData: Mappable {
    
    var current_page:Int?
    var first_page_url:String?
    var from:Int?
    var last_page:Int?
    var last_page_url:String?
    var next_page_url:String?
    var path:String?
    var per_page:Int?
    var prev_page_url:String?
    var to:Int?
    var total:Int?
    var data:[FundsRaisedData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        first_page_url <- map["first_page_url"]
        from <- map["from"]
        last_page <- map["last_page"]
        last_page_url <- map["last_page_url"]
        next_page_url <- map["next_page_url"]
        path <- map["path"]
        per_page <- map["per_page"]
        prev_page_url <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
    }
    
}
class FundsRaisedData: Mappable {
    
    var user_name:String?
    var bite:String?
    var date:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        user_name <- map["user_name"]
        bite <- map["bite"]
        date <- map["date"]
        
    }
    
}
class GetStateData: Mappable {
    
    var data:[StateData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
    
}
class StateData: Mappable {
    
    var state_id:String?
    var state_name:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        state_id <- map["state_id"]
        state_name <- map["state_name"]
    }
    
}

class BuyBiteData: Mappable {
    
    var transaction_amount:String?
    var transaction_id:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        transaction_amount <- map["transaction_amount"]
        transaction_id <- map["transaction_id"]
    }
    
}
